/**
 * Global variables
 */
"use strict";

var userAgent = navigator.userAgent.toLowerCase(),
	initialDate = new Date(),
	$document = $(document),
	isIE = userAgent.indexOf("msie") != -1 ? parseInt(userAgent.split("msie")[1]) : userAgent.indexOf("trident") != -1 ? 11 : userAgent.indexOf("edge") != -1 ? 12 : false,

	plugins = {
		rdNavbar: $(".rd-navbar"),
		progressBar: $(".progress-bar"),
		responsiveTabs: $(".responsive-tabs"),
	};

/**
 * Initialize All Scripts
 */
$document.ready(function () {


	/**
	 * isScrolledIntoView
	 * @description  check the element whas been scrolled into the view
	 */
	function isScrolledIntoView(elem) {
		var $window = $(window);
		return elem.offset().top + elem.outerHeight() >= $window.scrollTop() && elem.offset().top <= $window.scrollTop() + $window.height();
	}


	/**
	 * Copyright Year
	 * @description  Evaluates correct copyright year
	 */
	var o = $("#copyright-year");
	if (o.length) {
		o.text(initialDate.getFullYear());
	}


	/**
	 * RD Navbar
	 * @description Enables RD Navbar plugin
	 */
	if (plugins.rdNavbar.length) {
		plugins.rdNavbar.RDNavbar({
			stickUpClone: (plugins.rdNavbar.attr("data-stick-up-clone")) ? plugins.rdNavbar.attr("data-stick-up-clone") === 'true' : false
		});
		if (plugins.rdNavbar.attr("data-body-class")) {
			document.body.className += ' ' + plugins.rdNavbar.attr("data-body-class");
		}
	}


	/**
	 * Responsive Tabs
	 * @description Enables Responsive Tabs plugin
	 */
	if (plugins.responsiveTabs.length > 0) {
		var i;
		for (i = 0; i < plugins.responsiveTabs.length; i++) {
			var responsiveTabsItem = $(plugins.responsiveTabs[i]);
			responsiveTabsItem.easyResponsiveTabs({
				type: responsiveTabsItem.attr("data-type") === "accordion" ? "accordion" : "default"
			});
		}
	}

	/*function Auction(){
	 var _this = this;
	 _this.btn =  $(".auctionBtn");
	 _this.up = $('.auctionPopup');
	 console.log()
	 _this.btn.on('click',function(){
	 _this.up.addClass('show')
	 })
	 }
	 var auction = new Auction()
	 */
	$("._lk_payment_btn").on('click', function () {

		$(this).toggleClass('active');

		$("._lk_payment_filter").slideToggle()
	})

	var page_detail = function () {
		var el = $("._page-detail");
		el.on('click', '._del', function () {
			var b = $(this).parents('tbody');
			b.find('._tr_' + $(this).data('id')).remove();
			var tr = b.find('tr');
			if (tr.first().hasClass('_empty')) {
				tr.show()
			}
		})
		function table_add(el, data) {
			var b = el.find('tbody');
			var tr = b.find('tr');
			// data = JSON.stringify(data);

			function gen_html(id, data) {
				var html = "<tr data-json='" + JSON.stringify(data) + "'  data-id='" + id + "'  class='_tr_" + id + "' ><td><a href=''>" + +data.fio +
					"</a></td>"
				if (data.phone) {
					html = html + "<td>" + data.phone + "</td>"
				}
				if (data.perc) {
					html = html + "<td>" + data.perc + "%" + "</td>"
				}

				return html + "<td style='width: 75px;'><div class='tr-del _del' data-id='" + id + "' ></div></td></tr>";
			}


			if (tr.first().data('id') == '-1') {
				tr.first().hide();
				b.prepend(gen_html(1, data));
			} else {
				b.prepend(gen_html(parseInt(tr.first().data('id')) + 1, data));
			}

		}

		if (el.length) {
			el.on('click', '._cl', function () {
				// var b = el.find("."+$(this).attr('target'))
				var w = $(this).parents('.f-wrap');
				var b = w.find("." + $(this).attr('target'));
				w.find('._bt').removeClass('active');
				w.find('._copy').addClass('active');
				w.attr('status', "false");
				b.each(function (index) {
					if (index != 0) {
						$(this).remove()
					} else {
						$(this).slideToggle();
						$(this).find('form')[0].reset();
					}
				})

			});

			el.on('click', '._bt', function () {


				var w = $(this).parents('.f-wrap');
				var b = w.find("." + $(this).attr('target'));
				var btn = $(this);
				if (w.attr('status') == 'true') {
					b.each(function (index) {
						var form_data = $(this).find('form').serializeArray();
						// b.data('status',false);
						// b.slideToggle();
						var st = true;
						var data = {};
						$.each(form_data, function (i, v) {
							data[v.name] = v.value;
							// if(!v.value.length){
							// 	st = false;
							// }
						});
						if (st) {
							table_add(
								el.find("." + btn.data('table')),
								data
							);

							w.attr('status', "false");
							btn.removeClass('active');
							w.find('._copy').addClass('active');

							if (index != 0) {
								$(this).remove()
							} else {
								$(this).slideToggle();
								$(this).find('form')[0].reset();
							}

						}


					})


				} else {
					// b.data('status',true);
					// b.slideToggle();
				}

			});
			el.on('click', '._copy', function () {


				var w = $(this).parents('.f-wrap');
				var b = el.find("." + $(this).attr('target')).first();

				if (w.attr('status') != 'false') {
					var h = b.clone();
					// var id = h.data('id');
					// h.attr('class','f-wrap');
					// h.data('id',id+1);
					h.find('form')[0].reset();

					el.find("." + $(this).attr('target')).last().after(h);
					// h.find('.bh').data('status',true)
				} else {
					w.attr('status', true);
					b.slideToggle();
					$(this).removeClass('active');
					w.find('._bt').addClass('active');
				}
			})
		}

	}()

	var tooltip = function () {
		var b = $("._tooltip");
		var el = $("._toolshow");
		el.mouseover(function () {
			b.show()
			b.css({
				top: $(this).position().top + $(this).height() + 14,
				left: $(this).position().left + $(this).width() / 2
			});
			b.text($(this).data('text'))
		})
		el.mouseleave(function () {
			b.hide();
		})
	}()

	// $( "#slider" ).slider({
	//
	// 	range: "min",
	// 	max: 255,
	// 	value: 127,
	//
	// 	create: function(event, ui) {
	// 		var v = $(this).slider('value')
	// 		console.log(v)
	// 		// $(this).find('.ui-slider-handle').each(function (o,i) {
	// 		// 	$(this).html( "<div>"+v[o]+"</div>"  );
	// 		// })
	// 		$(this).find('.ui-slider-handle').html( "<div>"+v+"</div>"  )
	// 	},
	// 	slide: function( event, ui ) {
	// 		// console.log(event)
	// 		// console.log(ui)
	// 		$(ui.handle).html( "<div>"+ui.value+"</div>"  );
	// 	}
	// });


	google.charts.load('current', {packages: ['corechart', 'line']});
	google.charts.setOnLoadCallback(drawBasic);

	function drawBasic() {

		var data = new google.visualization.DataTable();
		// data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
		data.addColumn('number', 'X');
		data.addColumn('number', 'Процент годовых');

		data.addRows([
				[0, 25],
				[45, 25],
				[45, 18],
				[60, 18],
				[60, 15],
				[150, 10],
				[200, 5],
				[200, 0]
			]
		);
		// function createCustomHTMLContent(x, y) {
		// 	return '<div style="padding:5px 5px 5px 5px;">' +
		// 			'<div>Процент годовых:'+x+'</div>'
		// 		 + '</div>';
		// }
		// data.addRows([
		// 	[createCustomHTMLContent(0,0), 0,0],
		// 	[createCustomHTMLContent( 0,50), 0,50],
		// 	[createCustomHTMLContent(50,30), 50,30]
		// ]);
		var options = {
			lineWidth: 3,
			colors: ['#229dcf'],
			tooltip: {textStyle: {color: '#333333', fontSize: 14}},
			legend: {position: 'none'},
			height: 306,
			// height:320,
			chartArea: {top: 8, width: 583, height: '75%', left: 78},
			// chartArea:{top:8,width:250,height:'75%',left:105},
			crosshair: {
				color: "#fdc45b",
				trigger: "both"
			},
			hAxis: {
				color: "#f2f2f5",
				title: 'Дни',
				gridlines: {
					color: "#ffffff"
				},
				baselineColor: '#262626',
				titleTextStyle: {color: '#888c94', fontSize: 14, italic: false},
				// legendTextStyle: {color:'#f2f2f5'},
				textStyle: {color: '#333333', fontSize: 14}
			},
			vAxis: {
				color: "#f2f2f5",
				title: 'Процент  годовых',
				gridlines: {
					color: "#f2f2f5"
				},

				baselineColor: '#262626',
				titleTextStyle: {color: '#888c94', fontSize: 14, italic: false},
				// legendTextStyle: {color:'#f2f2f5'},
				textStyle: {color: '#333333', fontSize: 14}
			}
		};
		if ($('#chart_div').length) {
			var chart = new google.visualization.LineChart($('#chart_div')[0]);
			chart.draw(data, options);
		}


	}

	var date = function () {
		var c = $("._page-scf-join");
		var el = c.find('._date');
		var btn = c.find('._date-btn');
		var pop = c.find('._date-pop');

		function addDays(date, days) {
			var result = new Date(date);
			result.setDate(result.getDate() + days);
			return result;
		}

		var _MS_PER_DAY = 1000 * 60 * 60 * 24;

		function dateDiffInDays(a, b) {
			var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
			var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

			return Math.floor((utc2 - utc1) / _MS_PER_DAY);
		}

		el.datepicker({
			minDate: new Date(),
			maxDate: addDays(new Date(), 180),
			onSelect: function (formattedDate, date, inst) {
				// slider[2].el[0].noUiSlider.set(dateDiffInDays(new Date(),date));
				pop.toggleClass('active')
			}
		});


		btn.on('click', function () {
			pop.toggleClass('active')
		})


	}();
	var page_scf_join = function () {
		var c = $("._page-scf-join");
		if (c.length) {


			var s = c.find('#slider')
			if (s.length) {
				noUiSlider.create(s[0], {
					start: 34,
					step: 1,
					range: {
						'min': 0,
						'max': 100
					},
				});
				s[0].noUiSlider.on('update', function (values, handle) {
					// v.html();
					// console.log(values)
					s.find('.noUi-handle').html("<div class='noUi-handle-value'><b>" + parseInt(values[handle]) + "</b></div>")
				});
			}
		}
	}()


	$("._paper_lot").find('._slider').each(function () {
		var s = $(this).find('._s')
		var v = $(this).find('._v')
		noUiSlider.create(s[0], {
			start: 34,
			step: 1,
			range: {
				'min': 0,
				'max': 100
			},
		});
		s[0].noUiSlider.on('update', function (values, handle) {
			// v.html();
			// console.log(values)
			s.find('.noUi-handle').html("<div class='noUi-handle-value'>" + parseInt(values[handle]) + " %" + "</div>")
		});
	})


	// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
	if (!String.prototype.trim) {
		(function () {
			// Make sure we trim BOM and NBSP
			var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			String.prototype.trim = function () {
				return this.replace(rtrim, '');
			};
		})();
	}

	var y_input2 = function () {
		var i = $("input.y-input__field");
		var it = $("textarea.y-input__field");
		i.parents('.range').addClass('fix-input');
		it.parents('.range').addClass('fix-input');

		$.fn.selectRange = function (start, end) {
			if (end === undefined) {
				end = start;
			}
			return this.each(function () {
				if ('selectionStart' in this) {
					this.selectionStart = start;
					this.selectionEnd = end;
				} else if (this.setSelectionRange) {
					this.setSelectionRange(start, end);
				} else if (this.createTextRange) {
					var range = this.createTextRange();
					range.collapse(true);
					range.moveEnd('character', end);
					range.moveStart('character', start);
					range.select();
				}
			});
		};

		function fill(p) {
			p.addClass('y-input--filled')
		}

		function foc(p) {
			p.addClass('y-input--focus')
		}

		function de_fill(p) {
			p.removeClass('y-input--filled')
		}

		function de_foc(p) {
			p.removeClass('y-input--focus')
		}

		function place(_t, p, pl, placeholder) {
			var v = _t.val();
			var a = v.slice(0, v.length);
			var b = placeholder.slice(v.length, placeholder.length)
			var nl = a + b;
			pl.val(nl)
		}

		function bin(a) {
			var _t = $(a);
			var p = $(a).parents('.y-input');
			var pl = p.find('.y-input__placeholder');
			var placeholder = p.attr('data-pl');
			var sw = p.attr('data-sw');
			var helper = p.find('.y-input__helper').find('.c');
			var min_length = p.attr('data-min-length');
			if(!min_length){
				min_length = 0;
			}
			if(!sw){
				sw = '';
			}
			if (_t.val()) {
				fill(p)
			}
			if (p.attr('data-msk')) {
				_t.mask(p.attr('data-msk'))
			}

			function err() {
				if(min_length>0){
				if(_t.val().length>min_length){
					p.removeClass('y-input--error')
				}else{
					p.addClass('y-input--error')
				}
				}
				helper.text(_t.val().length+'/'+min_length);
			}
			_t.focus(function () {
				fill(p)
				foc(p)

				if (!_t.val()) {

					_t.val(sw)

					if (placeholder) {

						place(_t, p, pl, placeholder)
					}
				}
			});
			_t.blur(function () {
				if (!_t.val()) {
					de_fill(p)
				}
				if (_t.val() == p.attr('data-sw')) {
					_t.val('');
					pl.val('');
					de_fill(p)
				}
				de_foc(p)
			});
			_t.on('keydown', function (e) {
				if (e.keyCode == 8 || e.keyCode == 46) {
					if (_t.val().length - 1 < sw.length) {
						return false
					}
				} else {
					if (_t.val() == sw) {
						_t[0].setSelectionRange(sw.length, sw.length);
					}
				}

			});

			_t.on('input', function (e) {
				if (placeholder) {
					place(_t, p, pl, placeholder)
				}
				if (_t.val().length < sw.length) {
					_t.val(sw)
				}
				err()
			});
		}

		if (i.length) {
			i.each(function () {
				bin(this)
			});

		}
		if (it.length) {
			it.each(function () {
				bin(this)
			});

		}

	}();
	$("span.y-select__field").parents('.range').addClass('fix-input');
	$("span.y-select__field").click(function () {
		var _this = $(this);
		var p = _this.parents('.y-input');
		p.addClass('y-input--filled')
		p.addClass('y-input--focus')
		p.mouseleave(function () {
			$(document).one("click", function () {
				p.removeClass('y-input--filled')
				p.removeClass('y-input--focus')
				p.off('mouseleave')
			});
		});
		p.find('.o').click(function () {
			var v = $(this).attr('data-value')
			// p.find('.y-input__label-content').attr('data-content',v)
			// p.find('.y-input__label-content').text(v);
			if (v == 'reset') {
				p.find('.y-select__val').val('');
				p.find('.y-input__field').text('');
				p.removeClass('y-select--filled');
				p.removeClass('y-input--filled')
				p.removeClass('y-input--focus')
			} else {
				p.find('.y-select__val').val(v);
				p.find('.y-input__field').text(v);
				p.addClass('y-select--filled');
				p.removeClass('y-input--filled')
				p.removeClass('y-input--focus')
			}
			p.off('mouseleave')

		})
	});
	if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position) {
			position = position || 0;
			return this.indexOf(searchString, position) === position;
		};
	}
	var y_auto = function () {
		$("input.y-input__auto").each(function () {
			var _t = $(this);
			var strict = false;
			var p = _t.parents('.y-input');
			_t.parents('.range').addClass('fix-input');
		
			if (_t.hasClass('y-input__auto--strict')) {
				strict = true;
			}

			_t.val(p.find('.y-select__val').val());
			var opt = p.find(".y-input__option");
			var clear = p.find('.y-input__clear')
			if (_t.val()) {
				p.addClass('y-input--filled');
				opt.hide();
				clear.show()
			}
			clear.on('click', function () {
				_t.val('')
				p.find('.y-select__val').val('');
				opt.hide();
				o.hide();
				p.removeClass('y-input--filled');
				p.removeClass('y-input--focus');
				$(this).hide()
			})
			_t.click(function () {
				var _this = _t;

				p.addClass('y-input--filled');
				p.addClass('y-input--focus');
				var o = p.find('.o');
				opt.hide();

				_this.on('input',function () {
					var v = _this.val();
					console.log(v)
					p.find('.y-select__val').val(v);
					v = v.toLowerCase();
					if (v) {
						o.hide();
						var st = false;
						o.each(function () {
							var ov = $(this).attr('data-value');
							ov = ov.toLowerCase();
							if (ov.startsWith(v)) {
								var t = $(this).text()
								$(this).html("<span class='text-blue'>" + t.substring(0, v.length + 1) + "</span>" + t.substring(v.length + 1))
								$(this).show();
								st = true;
							}
						});
						if (!st) {
							opt.hide();
						} else {
							opt.show();
						}
						clear.css({
							display: 'block'
						})
					} else {
						opt.hide();
						o.hide();
						clear.hide()
					}

				})

				p.mouseleave(function () {
					$(document).one("click", function () {
						opt.hide();
						p.removeClass('y-input--focus');
						if (!_this.val()) {
							p.removeClass('y-input--filled');
						}
						if (strict) {
							if (!p.find('.o[data-value="' + _t.val() + '"]').length) {
								_t.val('')
								p.find('.y-select__val').val('');
								p.removeClass('y-input--filled');
								opt.hide();
								o.hide();
								clear.hide()
							}
						}
						p.off('mouseleave')
					});
				});
				o.click(function () {
					var v = $(this).attr('data-value')
					// p.find('.y-input__label-content').attr('data-content',v)
					// p.find('.y-input__label-content').text(v);
					p.find('.y-select__val').val(v);
					p.find('.y-input__auto').val(v);
					// p.addClass('y-select--filled');
					// p.removeClass('y-input--filled');
					opt.hide();
					p.removeClass('y-input--focus');
					p.off('mouseleave')
				})
			})
		})
	}()

	function Y_file(i) {
		var _this = this;
		_this.input = i.find('.y-file__input');
		_this._t = _this.input;
		_this.p = _this._t.parents('.y-file');
		_this.dummy = _this.p.find('.y-file__dummy');
		_this.s = _this.p.find('.select');
		_this.l = _this.p.find('.label');
		_this.n = _this.p.find('.name');
		_this.btn = _this.p.find('.sbtn');
		_this.list = _this.p.find('.list')
		_this.filelist = [];
		_this.submit_btn = _this.p.find(".submit");

		_this.old = false;
		if (window.FormData == undefined) {
			_this.old = true;
			_this.formData = null;
		}else{
			_this.formData = new FormData();
		}
		if (_this.old == true) {
			_this._t.removeAttr('multiple')
			$(".submit_y_file").removeClass('submit_y_file');

		}
		_this.p.on('click', '.del', function () {
			if (_this.old == false) {
				var newl = [];
				var id = $(this).parents('.item').attr('data-i')
				for (var i = 0; i < _this.filelist.length; i++) {
					if (i != id) {
						newl.push(_this.filelist[i])
					}
				}
				_this.filelist = newl;
				gen()
			}else{
				$(this).parents('.item').remove();
			}
		});

		_this.btn.on('click', function () {
			_this._t.click()
		});
		_this._t.on("dragover", function (event) {
			event.preventDefault();
			event.stopPropagation();
			_this.p.addClass('dragging');
		});

		_this._t.on("dragleave", function (event) {
			event.preventDefault();
			event.stopPropagation();
			_this.p.removeClass('dragging');
		});
		_this._t.on("drop", function (event) {
			_this.p.removeClass('dragging');
		});
		var gen = function () {
			_this.list.empty()
			for (var i = 0; i < _this.filelist.length; i++) {
				var fh = "<div class='item ' data-i='" + i + "' data-id='" + _this.filelist[i].size + "_" + _this.filelist[i].lastModified + "'><span>" + _this.filelist[i].name + "</span><div class='del'></div>" + '' + "</div>"
				_this.list.append(
					fh
				);
			}
		};
		_this.sen = function () {
			_this.formData = new FormData();
			for (var i = 0; i < _this.filelist.length; i++) {
				_this.formData.append("file_" + i, _this.filelist[i]);
			}
			$.ajax({
				url: _this.p.attr('data-url'),
				data: _this.formData,
				cache: false,
				contentType: false,
				processData: false,
				type: 'POST',
				success: function (d) {
					console.log(d)
				},
				error: function (d) {
					console.log(d)
				}
			});
		};
		var c = 0;
		_this._t.change(function () {
			var same = [];
				if (_this.old == false) {
					var files = _this._t[0].files;
					if (files.length) {
						var f = files[files.length - 1];
						var size = f.size / 1000000;
						_this.p.addClass('y-file--filled');

						for (var i = 0; i < files.length; i++) {
							var same_st = false;
							for(var x = 0;x<_this.filelist.length;x++){
								if(_this.filelist[x].name == files[i].name){
									same.push(files[i].name);
									same_st = true;
								}
							}
							if(!same_st){
								_this.filelist.push(files[i]);
							}
						}
						gen();
						if (size < 10) {

						} else {

						}
					} else {
						_this.p.removeClass('y-file--filled')
					}
					_this._t.parents('form').get(0).reset()
				} else {
					if(_this._t.val()) {
						var val = _this._t.val().replace(/C:\\fakepath\\/i, '')
						_this.list.find('.item').each(function () {
							console.log($(this).text())
							console.log(val)
							if($(this).text() == val){

								same.push(val)
							}
						});
						if(!same.length) {
							var cloned = _this._t.clone(true);
							_this._t.appendTo(_this.list)
							var item = _this.list.find('input').last();
							item.removeClass('y-file__input')
							item.wrap('<div class="item"></div>');
							var p = item.parents('.item');
							p.find('input').attr('name', 'file_' + c);
							p.append('<span>' +  val + '</span>')
							p.append('<div class="del"></div>');
							c++;

							_this.p.find('.y-file__dummy').append(cloned)
							_this._t = _this.p.find('.y-file__input')
						}
					}
					_this._t.parents('form').get(0).reset()
				}
				if(same.length){
					_this.p.addClass('error')
					_this.l.hide()
					_this.n.text(same.join(', ')+' уже добавлен'+((same.length>2)?"ы":""))
					if(_this.n.height()+44>100){
						_this.dummy.height(_this.n.height())
						_this.n.addClass('big');
					}
					_this.n.show();
					setTimeout(function () {
						_this.p.removeClass('error')
						_this.l.show()
						_this.n.text("")
						_this.n.hide()
						_this.dummy.height('')
						_this.n.removeClass('big');
					},3000)
				}
			}
		)

	}

	var y_file = $(".y-file");
	if (y_file.length) {
		var y_file_obj = {};
		y_file.each(function (i, k) {
			y_file_obj[$(this).attr('data-id')] = new Y_file($(this));
		});
		$(".submit_y_file").on('click', function () {
			y_file_obj[$(this).attr('data-y-id')].sen()
		})
	}


	var format = function (a) {
		a = Math.ceil(a)
		return String(a).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
	}
	var sum_input = function () {
		var el = $("._sum_input")

		if (el.length) {
			var m = el.find("._minus");
			var p = el.find("._plus");
			var i = el.find("._hidden");
			var v = el.find("._val");
			var c = $('._sum_input_check')

			v.mask('000 000 000 000 000', {
				reverse: true,
				onChange: function (cep) {
				}
			});

			c.change(function () {
				var ch = c.prop("checked");
				if (!ch) {
					el.addClass('active')
					v.removeAttr('disabled')
				} else {
					el.removeClass('active')
					v.attr('disabled', 'disabled')
				}

			})
			// set_v()
		}
	}()
	$("._mask_phone").mask('+7(000) 000-00-00', {
		placeholder: "+7(___) ___-__-__"
	});


})
;


