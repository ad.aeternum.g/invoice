/**
 * Global variables
 */
"use strict";

var userAgent = navigator.userAgent.toLowerCase(),
    initialDate = new Date(),

    $document = $(document),
    $window = $(window),
    $html = $("html"),

    isDesktop = $html.hasClass("desktop"),
    isIE = userAgent.indexOf("msie") != -1 ? parseInt(userAgent.split("msie")[1]) : userAgent.indexOf("trident") != -1 ? 11 : userAgent.indexOf("edge") != -1 ? 12 : false,
    isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
    isTouch = "ontouchstart" in window,

    plugins = {
        pointerEvents: isIE < 11 ? "js/pointer-events.min.js" : false,
        smoothScroll: $html.hasClass("use--smoothscroll") ? "js/smoothscroll.min.js" : false,
        bootstrapTooltip: $("[data-toggle='tooltip']"),
        bootstrapTabs: $(".tabs"),
        rdParallax: $(".rd-parallax"),
        rdAudioPlayer: $(".rd-audio"),
        rdVideoPlayer: $(".rd-video-player"),
        responsiveTabs: $(".responsive-tabs"),
        rdGoogleMaps: $("#rd-google-map"),
        rdNavbar: $(".rd-navbar"),
        rdVideoBG: $(".rd-video"),
        rdRange: $('.rd-range'),
        textRotator: $(".text-rotator"),
        owl: $(".owl-carousel"),
        swiper: $(".swiper-slider"),
        counter: $(".counter"),
        mfp: $('[data-lightbox]').not('[data-lightbox="gallery"] [data-lightbox]'),
        mfpGallery: $('[data-lightbox^="gallery"]'),
        flickrfeed: $(".flickr"),
        twitterfeed: $(".twitter"),
        // progressBar: $(".progress-bar"),
        isotope: $(".isotope"),
        countDown: $(".countdown"),
        calendar: $(".rd-calendar"),
        facebookfeed: $(".facebook"),
        instafeed: $(".instafeed"),
        facebookWidget: $('#fb-root'),
        materialTabs: $('.rd-material-tabs'),
        filePicker: $('.rd-file-picker'),
        fileDrop: $('.rd-file-drop'),
        popover: $('[data-toggle="popover"]'),
        dateCountdown: $('.DateCountdown'),
        statefulButton: $('.btn-stateful'),
        slick: $('.slick-slider'),
        scroller: $(".scroll-wrap"),
        socialite: $(".socialite"),
        viewAnimate: $('.view-animate'),
        selectFilter: $("select"),
        rdInputLabel: $(".form-label"),
        stacktable: $("[data-responsive=true]"),
        bootstrapDateTimePicker: $("[date-time-picker]"),
        customWaypoints: $('[data-custom-scroll-to]'),
        photoSwipeGallery: $("[data-photo-swipe-item]"),
        circleProgress: $(".progress-bar-circle"),
        stepper: $("input[type='number']"),
        radio: $("input[type='radio']"),
        checkbox: $("input[type='checkbox']"),
        customToggle: $("[data-custom-toggle]"),
        rdMailForm: $(".rd-mailform"),
        regula: $("[data-constraints]"),
        search: $(".rd-search"),
        searchResults: $('.rd-search-results'),
        imgZoom: $('[mag-thumb]'),
        calcSlider1: $(".calc-slider-1"),
        owlForms: $(".owl-forms-carousel"),
        owlLkReg: $(".owl-lk-reg"),
        hwp1: $("._hwp_1"),
        hwp_quest: $("._hwp_quest")
    };

/**
 * Initialize All Scripts
 */
$document.ready(function () {

    /**
     * getSwiperHeight
     * @description  calculate the height of swiper slider basing on data attr
     */
    function getSwiperHeight(object, attr) {
        var val = object.attr("data-" + attr),
            dim;

        if (!val) {
            return undefined;
        }

        dim = val.match(/(px)|(%)|(vh)$/i);

        if (dim.length) {
            switch (dim[0]) {
                case "px":
                    return parseFloat(val);
                case "vh":
                    return $(window).height() * (parseFloat(val) / 100);
                case "%":
                    return object.width() * (parseFloat(val) / 100);
            }
        } else {
            return undefined;
        }
    }

    function scrollFounders() {

        $('.page-founders__more').click(function () {
            $('html, body').stop().animate({
                scrollTop: $('a[name="' + $(this).attr('href').split('#')[1] + '"]').offset().top - 160
            }, 400);
            return false;
        });
    }

    scrollFounders();
    /**
     * toggleSwiperInnerVideos
     * @description  toggle swiper videos on active slides
     */
    function toggleSwiperInnerVideos(swiper) {
        var prevSlide = $(swiper.slides[swiper.previousIndex]),
            nextSlide = $(swiper.slides[swiper.activeIndex]),
            videos;

        prevSlide.find("video").each(function () {
            this.pause();
        });

        videos = nextSlide.find("video");
        if (videos.length) {
            videos.get(0).play();
        }
    }

    /**
     * toggleSwiperCaptionAnimation
     * @description  toggle swiper animations on active slides
     */
    function toggleSwiperCaptionAnimation(swiper) {
        var prevSlide = $(swiper.container),
            nextSlide = $(swiper.slides[swiper.activeIndex]);

        prevSlide
            .find("[data-caption-animate]")
            .each(function () {
                var $this = $(this);
                $this
                    .removeClass("animated")
                    .removeClass($this.attr("data-caption-animate"))
                    .addClass("not-animated");
            });

        nextSlide
            .find("[data-caption-animate]")
            .each(function () {
                var $this = $(this),
                    delay = $this.attr("data-caption-delay");

                setTimeout(function () {
                    $this
                        .removeClass("not-animated")
                        .addClass($this.attr("data-caption-animate"))
                        .addClass("animated");
                }, delay ? parseInt(delay) : 0);
            });
    }

    /**
     * makeParallax
     * @description  create swiper parallax scrolling effect
     */
    function makeParallax(el, speed, wrapper, prevScroll) {
        var scrollY = window.scrollY || window.pageYOffset;

        if (prevScroll != scrollY) {
            prevScroll = scrollY;
            el.addClass('no-transition');
            el[0].style['transform'] = 'translate3d(0,' + -scrollY * (1 - speed) + 'px,0)';
            el.height();
            el.removeClass('no-transition');

            if (el.attr('data-fade') === 'true') {
                var bound = el[0].getBoundingClientRect(),
                    offsetTop = bound.top * 2 + scrollY,
                    sceneHeight = wrapper.outerHeight(),
                    sceneDevider = wrapper.offset().top + sceneHeight / 2.0,
                    layerDevider = offsetTop + el.outerHeight() / 2.0,
                    pos = sceneHeight / 6.0,
                    opacity;
                if (sceneDevider + pos > layerDevider && sceneDevider - pos < layerDevider) {
                    el[0].style["opacity"] = 1;
                } else {
                    if (sceneDevider - pos < layerDevider) {
                        opacity = 1 + ((sceneDevider + pos - layerDevider) / sceneHeight / 3.0 * 5);
                    } else {
                        opacity = 1 - ((sceneDevider - pos - layerDevider) / sceneHeight / 3.0 * 5);
                    }
                    el[0].style["opacity"] = opacity < 0 ? 0 : opacity > 1 ? 1 : opacity.toFixed(2);
                }
            }
        }

        requestAnimationFrame(function () {
            makeParallax(el, speed, wrapper, prevScroll);
        });
    }

    /**
     * isScrolledIntoView
     * @description  check the element whas been scrolled into the view
     */
    function isScrolledIntoView(elem) {
        var $window = $(window);
        return elem.offset().top + elem.outerHeight() >= $window.scrollTop() && elem.offset().top <= $window.scrollTop() + $window.height();
    }

    /**
     * initOnView
     * @description  calls a function when element has been scrolled into the view
     */
    function lazyInit(element, func) {
        var $win = jQuery(window);
        $win.on('load scroll', function () {
            if ((!element.hasClass('lazy-loaded') && (isScrolledIntoView(element)))) {
                func.call();
                element.addClass('lazy-loaded');
            }
        });
    }

    /**
     * Live Search
     * @description  create live search results
     */
    function liveSearch(options) {
        $('#' + options.live).removeClass('cleared').html();
        options.current++;
        options.spin.addClass('loading');
        $.get(handler, {
            s: decodeURI(options.term),
            liveSearch: options.live,
            dataType: "html",
            liveCount: options.liveCount,
            filter: options.filter,
            template: options.template
        }, function (data) {
            options.processed++;
            var live = $('#' + options.live);
            if (options.processed == options.current && !live.hasClass('cleared')) {
                live.find('> #search-results').removeClass('active');
                live.removeClass('active');
                live.html(data);
                setTimeout(function () {
                    live.find('> #search-results').addClass('active');
                    live.addClass('active');
                }, 50);
            }
            options.spin.parents('.rd-search').find('.input-group-addon').removeClass('loading');
        })
    }

    /**
     * attachFormValidator
     * @description  attach form validation to elements
     */
    function attachFormValidator(elements) {
        for (var i = 0; i < elements.length; i++) {
            var o = $(elements[i]), v;
            o.addClass("form-control-has-validation").after("<span class='form-validation'></span>");
            v = o.parent().find(".form-validation");
            if (v.is(":last-child")) {
                o.addClass("form-control-last-child");
            }
        }

        elements
            .on('input change propertychange blur', function (e) {
                var $this = $(this), results;

                if (e.type != "blur") {
                    if (!$this.parent().hasClass("has-error")) {
                        return;
                    }
                }

                if ($this.parents('.rd-mailform').hasClass('success')) {
                    return;
                }

                if ((results = $this.regula('validate')).length) {
                    for (i = 0; i < results.length; i++) {
                        $this.siblings(".form-validation").text(results[i].message).parent().addClass("has-error")
                    }
                } else {
                    $this.siblings(".form-validation").text("").parent().removeClass("has-error")
                }
            })
            .regula('bind');
    }

    /**
     * isValidated
     * @description  check if all elemnts pass validation
     */
    function isValidated(elements) {
        var results, errors = 0;
        if (elements.length) {
            for (j = 0; j < elements.length; j++) {

                var $input = $(elements[j]);

                if ((results = $input.regula('validate')).length) {
                    for (k = 0; k < results.length; k++) {
                        errors++;
                        $input.siblings(".form-validation").text(results[k].message).parent().addClass("has-error");
                    }
                } else {
                    $input.siblings(".form-validation").text("").parent().removeClass("has-error")
                }
            }

            return errors == 0;
        }
        return true;
    }

    /**
     * Init Bootstrap tooltip
     * @description  calls a function when need to init bootstrap tooltips
     */
    function initBootstrapTooltip(tooltipPlacement) {
        if (window.innerWidth < 599) {
            plugins.bootstrapTooltip.tooltip('destroy');
            plugins.bootstrapTooltip.tooltip({
                placement: 'bottom'
            });
        } else {
            plugins.bootstrapTooltip.tooltip('destroy');
            plugins.bootstrapTooltip.tooltipPlacement;
            plugins.bootstrapTooltip.tooltip();
        }
    }

    /**
     * Copyright Year
     * @description  Evaluates correct copyright year
     */
    var o = $("#copyright-year");
    if (o.length) {
        o.text(initialDate.getFullYear());
    }

    /**
     * IE Polyfills
     * @description  Adds some loosing functionality to IE browsers
     */
    if (isIE) {
        if (isIE < 10) {
            $html.addClass("lt-ie-10");
        }

        if (isIE < 11) {
            if (plugins.pointerEvents) {
                $.getScript(plugins.pointerEvents)
                    .done(function () {
                        $html.addClass("ie-10");
                        PointerEventsPolyfill.initialize({});
                    });
            }
        }

        if (isIE === 11) {
            $("html").addClass("ie-11");
        }

        if (isIE === 12) {
            $("html").addClass("ie-edge");
        }
    }

    /**
     * Bootstrap Tooltips
     * @description Activate Bootstrap Tooltips
     */
    if (plugins.bootstrapTooltip.length) {
        var tooltipPlacement = plugins.bootstrapTooltip.attr('data-placement');
        initBootstrapTooltip(tooltipPlacement);
        $(window).on('resize orientationchange', function () {
            initBootstrapTooltip(tooltipPlacement);
        })
    }

    /**
     * Smooth scrolling
     * @description  Enables a smooth scrolling for Google Chrome (Windows)
     */
    if (plugins.smoothScroll) {
        $.getScript(plugins.smoothScroll);
    }

    /**
     * RD Audio player
     * @description Enables RD Audio player plugin
     */
    if (plugins.rdAudioPlayer.length > 0) {
        var i;
        for (i = 0; i < plugins.rdAudioPlayer.length; i++) {
            $(plugins.rdAudioPlayer[i]).RDAudio();
        }
    }

    /**
     * Text Rotator
     * @description Enables Text Rotator plugin
     */
    if (plugins.textRotator.length) {
        var i;
        for (i = 0; i < plugins.textRotator.length; i++) {
            var textRotatorItem = plugins.textRotator[i];
            $(textRotatorItem).rotator();
        }
    }

    /**
     * @module       Magnific Popup
     * @author       Dmitry Semenov
     * @see          http://dimsemenov.com/plugins/magnific-popup/
     * @version      v1.0.0
     */
    if (plugins.mfp.length > 0 || plugins.mfpGallery.length > 0) {
        if (plugins.mfp.length) {
            for (i = 0; i < plugins.mfp.length; i++) {
                var mfpItem = plugins.mfp[i];

                $(mfpItem).magnificPopup({
                    type: mfpItem.getAttribute("data-lightbox")
                });
            }
        }
        if (plugins.mfpGallery.length) {
            for (i = 0; i < plugins.mfpGallery.length; i++) {
                var mfpGalleryItem = $(plugins.mfpGallery[i]).find('[data-lightbox]');

                for (var c = 0; c < mfpGalleryItem.length; c++) {
                    $(mfpGalleryItem).addClass("mfp-" + $(mfpGalleryItem).attr("data-lightbox"));
                }

                mfpGalleryItem.end()
                    .magnificPopup({
                        delegate: '[data-lightbox]',
                        type: "image",
                        gallery: {
                            enabled: true
                        }
                    });
            }
        }
    }

    /**
     * RD Google Maps
     * @description Enables RD Google Maps plugin
     */
    if (plugins.rdGoogleMaps.length) {
        $.getScript("//maps.google.com/maps/api/js?sensor=false&libraries=geometry,places&v=3.7", function () {
            var head = document.getElementsByTagName('head')[0],
                insertBefore = head.insertBefore;

            head.insertBefore = function (newElement, referenceElement) {
                if (newElement.href && newElement.href.indexOf('//fonts.googleapis.com/css?family=Roboto') != -1 || newElement.innerHTML.indexOf('gm-style') != -1) {
                    return;
                }
                insertBefore.call(head, newElement, referenceElement);
            };

            lazyInit(plugins.rdGoogleMaps, function () {
                var styles = plugins.rdGoogleMaps.attr("data-styles");

                plugins.rdGoogleMaps.googleMap({
                    styles: styles ? JSON.parse(styles) : [],
                    onInit: function (map) {
                        var inputAddress = $('#rd-google-map-address');

                        if (inputAddress.length) {
                            var input = inputAddress;
                            var geocoder = new google.maps.Geocoder();
                            var marker = new google.maps.Marker(
                                {
                                    map: map,
                                    icon: "images/gmap_marker.png",
                                }
                            );
                            var autocomplete = new google.maps.places.Autocomplete(inputAddress[0]);
                            autocomplete.bindTo('bounds', map);
                            inputAddress.attr('placeholder', '');
                            inputAddress.on('change', function () {
                                $("#rd-google-map-address-submit").trigger('click');
                            });
                            inputAddress.on('keydown', function (e) {
                                if (e.keyCode == 13) {
                                    $("#rd-google-map-address-submit").trigger('click');
                                }
                            });


                            $("#rd-google-map-address-submit").on('click', function (e) {
                                e.preventDefault();
                                var address = input.val();
                                geocoder.geocode({'address': address}, function (results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var latitude = results[0].geometry.location.lat();
                                        var longitude = results[0].geometry.location.lng();

                                        map.setCenter(new google.maps.LatLng(
                                            parseFloat(latitude),
                                            parseFloat(longitude)
                                        ));
                                        marker.setPosition(new google.maps.LatLng(
                                            parseFloat(latitude),
                                            parseFloat(longitude)
                                        ))
                                    }
                                });
                            });
                        }
                    }
                })
            });
        });
    }

    /**
     * Bootstrap Date time picker
     */
    if (plugins.bootstrapDateTimePicker.length) {
        var i;
        for (i = 0; i < plugins.bootstrapDateTimePicker.length; i++) {
            var $dateTimePicker = $(plugins.bootstrapDateTimePicker[i]);
            var options = {};

            options['format'] = 'dddd DD MMMM YYYY - HH:mm';
            if ($dateTimePicker.attr("date-time-picker") == "date") {
                options['format'] = 'dddd DD MMMM YYYY';
                options['minDate'] = new Date();
            } else if ($dateTimePicker.attr("date-time-picker") == "time") {
                options['format'] = 'HH:mm';
            }

            options["time"] = ($dateTimePicker.attr("date-time-picker") != "date");
            options["date"] = ($dateTimePicker.attr("date-time-picker") != "time");
            options["shortTime"] = true;

            $dateTimePicker.bootstrapMaterialDatePicker(options);
        }
    }

    /**
     * Responsive Tabs
     * @description Enables Responsive Tabs plugin
     */
    if (plugins.responsiveTabs.length > 0) {
        var i;
        for (i = 0; i < plugins.responsiveTabs.length; i++) {
            var responsiveTabsItem = $(plugins.responsiveTabs[i]);
            responsiveTabsItem.easyResponsiveTabs({
                type: responsiveTabsItem.attr("data-type") === "accordion" ? "accordion" : "default"
            });
        }
    }

    /**
     * RD Instafeed
     * @description Enables Instafeed
     */
    if (plugins.instafeed.length > 0) {
        var i;
        for (i = 0; i < plugins.instafeed.length; i++) {
            var instafeedItem = $(plugins.instafeed[i]);
            instafeedItem.RDInstafeed({});
        }
    }

    /**
     * RD Twitter Feed
     * @description Enables RD Twitter Feed plugin
     */
    if (plugins.twitterfeed.length > 0) {
        var i;
        for (i = 0; i < plugins.twitterfeed.length; i++) {
            var twitterfeedItem = plugins.twitterfeed[i];
            $(twitterfeedItem).RDTwitter({});
        }
    }

    /**
     * RD MaterialTabs
     * @description Enables RD MaterialTabs plugin
     */
    if (plugins.materialTabs.length) {
        var i;
        for (i = 0; i < plugins.materialTabs.length; i++) {
            var materialTabsItem = plugins.materialTabs[i];
            $(materialTabsItem).RDMaterialTabs({});
        }
    }

    /**
     * RD Facebook
     * @description Enables RD Facebook plugin
     */
    if (plugins.facebookfeed.length > 0) {
        var i;
        for (i = 0; i < plugins.facebookfeed.length; i++) {
            var facebookfeedItem = plugins.facebookfeed[i];
            $(facebookfeedItem).RDFacebookFeed({});
        }
    }

    /**
     * Facebook widget
     * @description  Enables official Facebook widget
     */
    if (plugins.facebookWidget.length) {
        lazyInit(plugins.facebookWidget, function () {
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        });
    }

    /**
     * RD Flickr Feed
     * @description Enables RD Flickr Feed plugin
     */
    if (plugins.flickrfeed.length > 0) {
        var i;
        for (i = 0; i < plugins.flickrfeed.length; i++) {
            var flickrfeedItem = $(plugins.flickrfeed[i]);
            flickrfeedItem.RDFlickr({
                callback: function () {
                    var items = flickrfeedItem.find("[data-photo-swipe-item]");

                    if (items.length) {
                        for (var j = 0; j < items.length; j++) {
                            var image = new Image();
                            image.setAttribute('data-index', j);
                            image.onload = function () {
                                items[this.getAttribute('data-index')].setAttribute('data-size', this.naturalWidth + 'x' + this.naturalHeight);
                            };
                            image.src = items[j].getAttribute('href');
                        }
                    }
                }
            });
        }
    }

    /**
     * Select2
     * @description Enables select2 plugin
     */
    if (plugins.selectFilter.length) {
        var i;
        for (i = 0; i < plugins.selectFilter.length; i++) {
            var select = $(plugins.selectFilter[i]);

            select.select2({
                theme: "bootstrap"
            }).next().addClass(select.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
        }
    }

    /**
     * Stepper
     * @description Enables Stepper Plugin
     */
    if (plugins.stepper.length) {
        plugins.stepper.stepper({
            labels: {
                up: "",
                down: ""
            }
        });
    }

    /**
     * Radio
     * @description Add custom styling options for input[type="radio"]
     */
    if (plugins.radio.length) {
        //var i;
        //for (i = 0; i < plugins.radio.length; i++) {
        //	var $this = $(plugins.radio[i]);
        //	$this.addClass("radio-custom").after("<span class='radio-custom-dummy'></span>")
        //}
    }

    /**
     * Checkbox
     * @description Add custom styling options for input[type="checkbox"]
     */
    if (plugins.checkbox.length) {
        //var i;
        //for (i = 0; i < plugins.checkbox.length; i++) {
        //  var $this = $(plugins.checkbox[i]);
        //  $this.addClass("checkbox-custom").after("<span class='checkbox-custom-dummy'></span>")
        //}
    }

    /**
     * RD Filepicker
     * @description Enables RD Filepicker plugin
     */
    if (plugins.filePicker.length || plugins.fileDrop.length) {
        var i;
        for (i = 0; i < plugins.filePicker.length; i++) {
            var filePickerItem = plugins.filePicker[i];

            $(filePickerItem).RDFilepicker({
                metaFieldClass: "rd-file-picker-meta"
            });
        }

        for (i = 0; i < plugins.fileDrop.length; i++) {
            var fileDropItem = plugins.fileDrop[i];

            $(fileDropItem).RDFilepicker({
                metaFieldClass: "rd-file-drop-meta",
                buttonClass: "rd-file-drop-btn",
                dropZoneClass: "rd-file-drop"
            });
        }
    }

    /**
     * Popovers
     * @description Enables Popovers plugin
     */
    if (plugins.popover.length) {
        if (window.innerWidth < 767) {
            plugins.popover.attr('data-placement', 'bottom');
            plugins.popover.popover();
        }
        else {
            plugins.popover.popover();
        }
    }

    /**
     * jQuery Countdown
     * @description  Enable countdown plugin
     */
    if (plugins.countDown.length) {
        var i;
        for (i = 0; i < plugins.countDown.length; i++) {
            var countDownItem = plugins.countDown[i],
                d = new Date(),
                type = countDownItem.getAttribute('data-type'),
                time = countDownItem.getAttribute('data-time'),
                format = countDownItem.getAttribute('data-format'),
                settings = [];

            d.setTime(Date.parse(time)).toLocaleString();
            settings[type] = d;
            settings['format'] = format;
            $(countDownItem).countdown(settings);
        }
    }

    /**
     * @module TimeCircles
     * @author Wim Barelds
     * @version 1.5.3
     * @see http://www.wimbarelds.nl/
     * @license MIT License
     */
    if (plugins.dateCountdown.length) {
        for (i = 0; i < plugins.dateCountdown.length; i++) {
            var dateCountdownItem = $(plugins.dateCountdown[i]),
                time = {
                    "Days": {
                        "text": "Days",
                        "color": "#ffffff",
                        "show": true
                    },
                    "Hours": {
                        "text": "Hours",
                        "color": "#ffffff",
                        "show": true
                    },
                    "Minutes": {
                        "text": "Minutes",
                        "color": "#ffffff",
                        "show": true
                    },
                    "Seconds": {
                        "text": "Seconds",
                        "color": "#ffffff",
                        "show": true
                    }
                };
            dateCountdownItem.TimeCircles({
                "animation": "smooth",
                "bg_width": 0.5,
                "fg_width": 0.02,
                "circle_bg_color": dateCountdownItem.attr('data-bg') ? dateCountdownItem.attr('data-bg') : "rgba(255,255,255,.39)",
                "time": time
            });

            $(window).on('load resize orientationchange', (function ($dateCountdownItem, time) {
                return function () {
                    if (window.innerWidth < 479) {
                        $dateCountdownItem.TimeCircles({
                            time: {
                                Minutes: {show: true},
                                Seconds: {show: false}
                            }
                        }).rebuild();
                    } else if (window.innerWidth < 767) {
                        $dateCountdownItem.TimeCircles({
                            time: {
                                Seconds: {show: false}
                            }
                        }).rebuild();
                    } else {
                        $dateCountdownItem.TimeCircles({time: time}).rebuild();
                    }
                }

            })($(dateCountdownItem), time));
        }
    }

    /**
     * Bootstrap Buttons
     * @description  Enable Bootstrap Buttons plugin
     */
    if (plugins.statefulButton.length) {
        $(plugins.statefulButton).on('click', function () {
            var statefulButtonLoading = $(this).button('loading');

            setTimeout(function () {
                statefulButtonLoading.button('reset')
            }, 2000);
        })
    }

    /**
     * RD Calendar
     * @description Enables RD Calendar plugin
     */
    if (plugins.calendar.length) {
        var i;
        for (i = 0; i < plugins.calendar.length; i++) {
            var calendarItem = $(plugins.calendar[i]);

            calendarItem.rdCalendar({
                days: calendarItem.attr("data-days") ? calendarItem.attr("data-days").split(/\s?,\s?/i) : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                month: calendarItem.attr("data-months") ? calendarItem.attr("data-months").split(/\s?,\s?/i) : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            });
        }
    }

    /**
     * Circle Progress
     * @description Enable Circle Progress plugin
     */
    if (plugins.circleProgress.length) {
        var i;
        for (i = 0; i < plugins.circleProgress.length; i++) {
            var circleProgressItem = $(plugins.circleProgress[i]);
            $document
                .on("scroll", function () {
                    if (!circleProgressItem.hasClass('animated')) {

                        var arrayGradients = circleProgressItem.attr('data-gradient').split(",");

                        circleProgressItem.circleProgress({
                            value: circleProgressItem.attr('data-value'),
                            size: circleProgressItem.attr('data-size') ? circleProgressItem.attr('data-size') : 175,
                            fill: {gradient: arrayGradients, gradientAngle: Math.PI / 4},
                            startAngle: -Math.PI / 4 * 2,
                            emptyFill: $(this).attr('data-empty-fill') ? $(this).attr('data-empty-fill') : "rgb(245,245,245)"

                        }).on('circle-animation-progress', function (event, progress, stepValue) {
                            $(this).find('span').text(String(stepValue.toFixed(2)).replace('0.', '').replace('1.', '1'));
                        });
                        circleProgressItem.addClass('animated');
                    }
                })
                .trigger("scroll");
        }
    }

    /**
     * Progress bar
     * @description  Enable progress bar
     */
    // if (plugins.progressBar.length) {
    // 	var i,
    // 		bar,
    // 		type;
    // 	for (i = 0; i < plugins.progressBar.length; i++) {
    // 		var progressItem = plugins.progressBar[i];
    // 		bar = null;
    // 		if (progressItem.className.indexOf("progress-bar-horizontal") > -1) {
    // 			type = 'Line';
    // 		}
    //
    // 		if (progressItem.className.indexOf("progress-bar-radial") > -1) {
    // 			type = 'Circle';
    // 		}
    //
    // 		if (progressItem.getAttribute("data-stroke") && progressItem.getAttribute("data-value") && type) {
    // 			bar = new ProgressBar[type](progressItem, {
    // 				strokeWidth: Math.round(parseFloat(progressItem.getAttribute("data-stroke")) / progressItem.offsetWidth * 100)
    // 				,
    // 				trailWidth: progressItem.getAttribute("data-trail") ? Math.round(parseFloat(progressItem.getAttribute("data-trail")) / progressItem.offsetWidth * 100) : 0
    // 				,
    // 				text: {
    // 					value: progressItem.getAttribute("data-counter") === "true" ? '0' : null
    // 					, className: 'progress-bar__body'
    // 					, style: null
    // 				}
    // 			});
    // 			bar.svg.setAttribute('preserveAspectRatio', "none meet");
    // 			if (type === 'Line') {
    // 				bar.svg.setAttributeNS(null, "height", progressItem.getAttribute("data-stroke"));
    // 			}
    //
    // 			bar.path.removeAttribute("stroke");
    // 			bar.path.className.baseVal = "progress-bar__stroke";
    // 			if (bar.trail) {
    // 				bar.trail.removeAttribute("stroke");
    // 				bar.trail.className.baseVal = "progress-bar__trail";
    // 			}
    //
    // 			if (progressItem.getAttribute("data-easing") && !isIE) {
    // 				$(document)
    // 					.on("scroll", {"barItem": bar}, $.proxy(function (event) {
    // 						var bar = event.data.barItem;
    // 						if (isScrolledIntoView($(this)) && this.className.indexOf("progress-bar--animated") === -1) {
    // 							this.className += " progress-bar--animated";
    // 							bar.animate(parseInt(this.getAttribute("data-value")) / 100.0, {
    // 								easing: this.getAttribute("data-easing")
    // 								,
    // 								duration: this.getAttribute("data-duration") ? parseInt(this.getAttribute("data-duration")) : 800
    // 								,
    // 								step: function (state, b) {
    // 									if (b._container.className.indexOf("progress-bar-horizontal") > -1 ||
    // 										b._container.className.indexOf("progress-bar-vertical") > -1) {
    // 										b.text.style.width = Math.abs(b.value() * 100).toFixed(0) + "%"
    // 									}
    // 									b.setText(Math.abs(b.value() * 100).toFixed(0));
    // 								}
    // 							});
    // 						}
    // 					}, progressItem))
    // 					.trigger("scroll");
    // 			} else {
    // 				bar.set(parseInt($(progressItem).attr("data-value")) / 100.0);
    // 				bar.setText($(progressItem).attr("data-value"));
    // 				if (type === 'Line') {
    // 					bar.text.style.width = parseInt($(progressItem).attr("data-value")) + "%";
    // 				}
    // 			}
    // 		} else {
    // 			console.error(progressItem.className + ": progress bar type is not defined");
    // 		}
    // 	}
    // }

    /**
     * UI To Top
     * @description Enables ToTop Button
     */
    if (isDesktop) {
        $().UItoTop({
            easingType: 'easeOutQuart',
            containerClass: 'ui-to-top fa fa-angle-up'
        });
    }

    /**
     * RD Navbar
     * @description Enables RD Navbar plugin
     */
    if (plugins.rdNavbar.length) {
        plugins.rdNavbar.RDNavbar({
            stickUpClone: (plugins.rdNavbar.attr("data-stick-up-clone")) ? plugins.rdNavbar.attr("data-stick-up-clone") === 'true' : false
        });
        if (plugins.rdNavbar.attr("data-body-class")) {
            document.body.className += ' ' + plugins.rdNavbar.attr("data-body-class");
        }
    }

    /**
     * ViewPort Universal
     * @description Add class in viewport
     */
    if (plugins.viewAnimate.length) {
        var i;
        for (i = 0; i < plugins.viewAnimate.length; i++) {
            var $view = $(plugins.viewAnimate[i]).not('.active');
            $document.on("scroll", $.proxy(function () {
                if (isScrolledIntoView(this)) {
                    this.addClass("active");
                }
            }, $view))
                .trigger("scroll");
        }
    }

    /**
     * Swiper 3.1.7
     * @description  Enable Swiper Slider
     */
    if (plugins.swiper.length) {
        var i;
        for (i = 0; i < plugins.swiper.length; i++) {
            var s = $(plugins.swiper[i]);
            var pag = s.find(".swiper-pagination"),
                next = s.find(".swiper-button-next"),
                prev = s.find(".swiper-button-prev"),
                bar = s.find(".swiper-scrollbar"),
                parallax = s.parents('.rd-parallax').length,
                swiperSlide = s.find(".swiper-slide");

            for (j = 0; j < swiperSlide.length; j++) {
                var $this = $(swiperSlide[j]),
                    url;

                if (url = $this.attr("data-slide-bg")) {
                    $this.css({
                        "background-image": "url(" + url + ")",
                        "background-size": "cover"
                    })
                }
            }

            swiperSlide.end()
                .find("[data-caption-animate]")
                .addClass("not-animated")
                .end()
                .swiper({
                    autoplay: s.attr('data-autoplay') ? s.attr('data-autoplay') === "false" ? undefined : s.attr('data-autoplay') : 5000,
                    direction: s.attr('data-direction') ? s.attr('data-direction') : "horizontal",
                    effect: s.attr('data-slide-effect') ? s.attr('data-slide-effect') : "slide",
                    speed: s.attr('data-slide-speed') ? s.attr('data-slide-speed') : 600,
                    keyboardControl: s.attr('data-keyboard') === "true",
                    mousewheelControl: s.attr('data-mousewheel') === "true",
                    mousewheelReleaseOnEdges: s.attr('data-mousewheel-release') === "true",
                    nextButton: next.length ? next.get(0) : null,
                    prevButton: prev.length ? prev.get(0) : null,
                    pagination: pag.length ? pag.get(0) : null,
                    paginationClickable: pag.length ? pag.attr("data-clickable") !== "false" : false,
                    paginationBulletRender: pag.length ? pag.attr("data-index-bullet") === "true" ? function (index, className) {
                        return '<span class="' + className + '">' + (index + 1) + '</span>';
                    } : null : null,
                    scrollbar: bar.length ? bar.get(0) : null,
                    scrollbarDraggable: bar.length ? bar.attr("data-draggable") !== "false" : true,
                    scrollbarHide: bar.length ? bar.attr("data-draggable") === "false" : false,
                    loop: s.attr('data-loop') !== "false",
                    onTransitionStart: function (swiper) {
                        toggleSwiperInnerVideos(swiper);
                    },
                    onTransitionEnd: function (swiper) {
                        toggleSwiperCaptionAnimation(swiper);
                    },
                    onInit: function (swiper) {
                        toggleSwiperInnerVideos(swiper);
                        toggleSwiperCaptionAnimation(swiper);
                        var swiperParalax = s.find(".swiper-parallax");

                        for (var k = 0; k < swiperParalax.length; k++) {
                            var $this = $(swiperParalax[k]),
                                speed;

                            if (parallax && !isIEBrows && !isMobile) {
                                if (speed = $this.attr("data-speed")) {
                                    makeParallax($this, speed, s, false);
                                }
                            }
                        }
                        $(window).on('resize', function () {
                            swiper.update(true);
                        })
                    }
                });

            $(window)
                .on("resize", function () {
                    var mh = getSwiperHeight(s, "min-height"),
                        h = getSwiperHeight(s, "height");
                    if (h) {
                        s.css("height", mh ? mh > h ? mh : h : h);
                    }
                })
                .trigger("resize");
        }
    }

    /**
     * RD Video Player
     * @description Enables RD Video player plugin
     */
    if (plugins.rdVideoPlayer.length) {
        var i;
        for (i = 0; i < plugins.rdVideoPlayer.length; i++) {
            var videoItem = plugins.rdVideoPlayer[i],
                volumeWrap = $(".rd-video-volume-wrap");

            $(videoItem).RDVideoPlayer({});

            volumeWrap.on("mouseenter", function () {
                $(this).addClass("hover")
            });

            volumeWrap.on("mouseleave", function () {
                $(this).removeClass("hover")
            });

            if (isTouch) {
                volumeWrap.find(".rd-video-volume").on("click", function () {
                    $(this).toggleClass("hover")
                });
                $document.on("click", function (e) {
                    if (!$(e.target).is(volumeWrap) && $(e.target).parents(volumeWrap).length == 0) {
                        volumeWrap.find(".rd-video-volume").removeClass("hover")
                    }
                })
            }
        }
    }

    /**
     * RD Parallax
     * @description Enables RD Parallax plugin
     */
    if (plugins.rdParallax.length) {
        var i;


        // if (!isIE && !isMobile) {
        //
        // 	$.RDParallax();
        // 	$(window).on("scroll", function () {
        //
        // 		for (i = 0; i < plugins.rdParallax.length; i++) {
        // 			var parallax = $(plugins.rdParallax[i]);
        // 			console.log()
        // 			if (isScrolledIntoView(parallax)) {
        //
        // 				parallax.find(".rd-parallax-inner").css("position", "fixed");
        // 				// parallax.find(".rd-parallax-inner").css("marginTop", "-10px");
        // 				// parallax.find(".rd-parallax-inner").css("padding", "10px 0 0 0");
        // 			} else {
        // 				// parallax.find(".rd-parallax-inner").css("position", "absolute");
        // 				// parallax.find(".rd-parallax-layer").css("position", "relative");
        // 				// parallax.find(".rd-parallax-layer-holder").css("position", "relative");
        // 			}
        // 		}
        // 	});
        // }else{
        //
        //
        // }

        $(".rd-parallax").each(function () {
            $(this).css({
                position: 'relative'
            })
            var h = $(this).outerHeight();
            var l = $(this).find('div[data-type="media"]');
            var c = $(this).find('div[data-type="html"]');
            l.css({
                height: h,
                backgroundImage: 'url("' + l.attr('data-url') + '")'
            });
            c.css({
                zIndex: 10,
                position: 'relative'
            });

        })

        $("a[href='#']").on("click", function (event) {
            setTimeout(function () {
                $(window).trigger("resize");
            }, 300);
        });
    }

    /**
     * RD Search
     * @description Enables search
     */
    if (plugins.search.length || plugins.searchResults) {
        var handler = "bat/rd-search.php";
        var defaultTemplate = '<h5 class="search_title"><a target="_top" href="#{href}" class="search_link">#{title}</a></h5>' +
            '<p>...#{token}...</p>' +
            '<p class="match"><em>Terms matched: #{count} - URL: #{href}</em></p>';
        var defaultFilter = '*.html';

        if (plugins.search.length) {

            for (i = 0; i < plugins.search.length; i++) {
                var searchItem = $(plugins.search[i]),
                    options = {
                        element: searchItem,
                        filter: (searchItem.attr('data-search-filter')) ? searchItem.attr('data-search-filter') : defaultFilter,
                        template: (searchItem.attr('data-search-template')) ? searchItem.attr('data-search-template') : defaultTemplate,
                        live: (searchItem.attr('data-search-live')) ? searchItem.attr('data-search-live') : false,
                        liveCount: (searchItem.attr('data-search-live-count')) ? parseInt(searchItem.attr('data-search-live')) : 4,
                        current: 0, processed: 0, timer: {}
                    };

                if ($('.rd-navbar-search-toggle').length) {
                    var toggle = $('.rd-navbar-search-toggle');
                    toggle.on('click', function () {
                        if (!($(this).hasClass('active'))) {
                            searchItem.find('input').val('').trigger('propertychange');
                        }
                    });
                }

                if (options.live) {
                    var clearHandler = false;

                    searchItem.find('input').on("keyup input propertychange", $.proxy(function () {
                        this.term = this.element.find('input').val().trim();
                        this.spin = this.element.find('.input-group-addon');

                        clearTimeout(this.timer);

                        if (this.term.length > 2) {
                            this.timer = setTimeout(liveSearch(this), 200);

                            if (clearHandler == false) {
                                clearHandler = true;

                                $("body").on("click", function (e) {
                                    if ($(e.toElement).parents('.rd-search').length == 0) {
                                        $('.rd-search-results-live').addClass('cleared').html('');
                                        $(this).unbind('click');
                                        clearHandler = false;
                                    }
                                })
                            }

                        } else if (this.term.length == 0) {
                            $('#' + this.live).addClass('cleared').html('');
                        }
                    }, options, this));
                }

                searchItem.submit($.proxy(function () {
                    $('<input />').attr('type', 'hidden')
                        .attr('name', "filter")
                        .attr('value', this.filter)
                        .appendTo(this.element);
                    return true;
                }, options, this))
            }
        }

        if (plugins.searchResults.length) {
            var regExp = /\?.*s=([^&]+)\&filter=([^&]+)/g;
            var match = regExp.exec(location.search);

            if (match != null) {
                $.get(handler, {
                    s: decodeURI(match[1]),
                    dataType: "html",
                    filter: match[2],
                    template: defaultTemplate,
                    live: ''
                }, function (data) {
                    plugins.searchResults.html(data);
                })
            }
        }
    }

    /**
     * Slick carousel
     * @description  Enable Slick carousel plugin
     */
    if (plugins.slick.length) {
        var i;
        for (i = 0; i < plugins.slick.length; i++) {
            var $slickItem = $(plugins.slick[i]);

            $slickItem.slick({
                slidesToScroll: parseInt($slickItem.attr('data-slide-to-scroll')) || 1,
                asNavFor: $slickItem.attr('data-for') || false,
                dots: $slickItem.attr("data-dots") == "true",
                infinite: $slickItem.attr("data-loop") == "true",
                focusOnSelect: true,
                arrows: $slickItem.attr("data-arrows") == "true",
                swipe: $slickItem.attr("data-swipe") == "true",
                autoplay: $slickItem.attr("data-autoplay") == "true",
                vertical: $slickItem.attr("data-vertical") == "true",
                mobileFirst: true,
                responsive: [
                    {
                        breakpoint: 0,
                        settings: {
                            slidesToShow: parseInt($slickItem.attr('data-items')) || 1,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: parseInt($slickItem.attr('data-xs-items')) || 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: parseInt($slickItem.attr('data-sm-items')) || 1,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: parseInt($slickItem.attr('data-md-items')) || 1,
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: parseInt($slickItem.attr('data-lg-items')) || 1,
                        }
                    }
                ]
            });
        }
    }

    /**
     * Owl carousel
     * @description Enables Owl carousel plugin
     */
    if (plugins.owl.length) {
        var i;
        for (i = 0; i < plugins.owl.length; i++) {
            var c = $(plugins.owl[i]),
                responsive = {};

            var aliaces = ["-", "-xs-", "-sm-", "-md-", "-lg-"],
                values = [0, 480, 768, 992, 1200],
                j, k;

            for (j = 0; j < values.length; j++) {
                responsive[values[j]] = {};
                for (k = j; k >= -1; k--) {
                    if (!responsive[values[j]]["items"] && c.attr("data" + aliaces[k] + "items")) {
                        responsive[values[j]]["items"] = k < 0 ? 1 : parseInt(c.attr("data" + aliaces[k] + "items"));
                    }
                    if (!responsive[values[j]]["stagePadding"] && responsive[values[j]]["stagePadding"] !== 0 && c.attr("data" + aliaces[k] + "stage-padding")) {
                        responsive[values[j]]["stagePadding"] = k < 0 ? 0 : parseInt(c.attr("data" + aliaces[k] + "stage-padding"));
                    }
                    if (!responsive[values[j]]["margin"] && responsive[values[j]]["margin"] !== 0 && c.attr("data" + aliaces[k] + "margin")) {
                        responsive[values[j]]["margin"] = k < 0 ? 30 : parseInt(c.attr("data" + aliaces[k] + "margin"));
                    }
                }
            }

            c.owlCarousel({
                autoplay: c.attr("data-autoplay") === "true",
                loop: c.attr("data-loop") !== "false",
                items: 1,
                dotsContainer: c.attr("data-pagination-class") || false,
                navContainer: c.attr("data-navigation-class") || false,
                mouseDrag: c.attr("data-mouse-drag") !== "false",
                nav: c.attr("data-nav") === "true",
                dots: c.attr("data-dots") === "true",
                dotsEach: c.attr("data-dots-each") ? parseInt(c.attr("data-dots-each")) : false,
                animateOut: c.attr("data-animation-out") || false,
                responsive: responsive,
                navText: []
            });
        }
    }

    /**
     * jQuery Count To
     * @description Enables Count To plugin
     */
    if (plugins.counter.length) {
        var i;
        for (i = 0; i < plugins.counter.length; i++) {
            var counterNotAnimated = plugins.counter.not(".animated");

            $document.on("scroll", function () {
                for (i = 0; i < counterNotAnimated.length; i++) {
                    var counterNotAnimatedItem = $(counterNotAnimated[i]);
                    if ((!counterNotAnimatedItem.hasClass("animated")) && (isScrolledIntoView(counterNotAnimatedItem))) {
                        counterNotAnimatedItem.countTo({
                            refreshInterval: 40,
                            speed: counterNotAnimatedItem.attr("data-speed") || 1000
                        });
                        counterNotAnimatedItem.addClass('animated');
                    }
                }
            });
            $document.trigger("scroll");
        }
    }

    /**
     * Isotope
     * @description Enables Isotope plugin
     */
    if (plugins.isotope.length) {
        var i, isogroup = [];
        for (i = 0; i < plugins.isotope.length; i++) {
            var isotopeItem = plugins.isotope[i]
                , iso = new Isotope(isotopeItem, {
                itemSelector: '.isotope-item',
                layoutMode: isotopeItem.getAttribute('data-isotope-layout') ? isotopeItem.getAttribute('data-isotope-layout') : 'masonry',
                filter: '*'
            });

            isogroup.push(iso);
        }

        $(window).on('load', function () {
            setTimeout(function () {
                var i;
                for (i = 0; i < isogroup.length; i++) {
                    isogroup[i].element.className += " isotope--loaded";
                    isogroup[i].layout();
                }
            }, 600);
        });

        var resizeTimout;

        $("[data-isotope-filter]").on("click", function (e) {
            e.preventDefault();
            var filter = $(this);
            clearTimeout(resizeTimout);
            filter.parents(".isotope-filters").find('.active').removeClass("active");
            filter.addClass("active");
            var iso = $('.isotope[data-isotope-group="' + this.getAttribute("data-isotope-group") + '"]');
            iso.isotope({
                itemSelector: '.isotope-item',
                layoutMode: iso.attr('data-isotope-layout') ? iso.attr('data-isotope-layout') : 'masonry',
                filter: this.getAttribute("data-isotope-filter") == '*' ? '*' : '[data-filter*="' + this.getAttribute("data-isotope-filter") + '"]'
            });
        }).eq(0).trigger("click")
    }

    /**
     * WOW
     * @description Enables Wow animation plugin
     */
    if (isDesktop && $html.hasClass("wow-animation") && $(".wow").length) {
        new WOW().init();
    }

    /**
     * Bootstrap tabs
     * @description Activate Bootstrap Tabs
     */
    if (plugins.bootstrapTabs.length) {
        var i;
        for (i = 0; i < plugins.bootstrapTabs.length; i++) {
            var bootstrapTabsItem = $(plugins.bootstrapTabs[i]);

            bootstrapTabsItem.on("click", "a", function (event) {
                event.preventDefault();
                $(this).tab('show');
            });
        }
    }

    /**
     * JQuery mousewheel plugin
     * @description  Enables jquery mousewheel plugin
     */
    if (plugins.scroller.length) {
        var i;
        for (i = 0; i < plugins.scroller.length; i++) {
            var scrollerItem = $(plugins.scroller[i]);

            scrollerItem.mCustomScrollbar({
                scrollInertia: 200,
                scrollButtons: {enable: true}
            });
        }
    }

    /**
     * Socialite v2
     * @description  Enables Socialite v2 plugin
     */
    if (plugins.socialite.length) {
        Socialite.load();
    }

    /**
     * RD Video
     * @description Enables RD Video plugin
     */
    if (plugins.rdVideoBG.length) {
        var i;
        for (i = 0; i < plugins.rdVideoBG.length; i++) {
            var videoItem = $(plugins.rdVideoBG[i]);
            videoItem.RDVideo({});
        }
    }

    /**
     * RD Input Label
     * @description Enables RD Input Label Plugin
     */
    if (plugins.rdInputLabel.length) {
        plugins.rdInputLabel.RDInputLabel();
    }

    /**
     * Regula
     * @description Enables Regula plugin
     */
    if (plugins.regula.length) {
        attachFormValidator(plugins.regula);
    }

    /**
     * RD Mailform
     */

    if (plugins.rdMailForm.length) {
        var i, j, k,
            msg = {
                'MF000': 'Successfully sent!',
                'MF001': 'Recipients are not set!',
                'MF002': 'Form will not work locally!',
                'MF003': 'Please, define email field in your form!',
                'MF004': 'Please, define type of your form!',
                'MF254': 'Something went wrong with PHPMailer!',
                'MF255': 'Aw, snap! Something went wrong.'
            };
        for (i = 0; i < plugins.rdMailForm.length; i++) {
            var $form = $(plugins.rdMailForm[i]);

            $form.attr('novalidate', 'novalidate').ajaxForm({
                data: {
                    "form-type": $form.attr("data-form-type") || "contact",
                    "counter": i
                },
                beforeSubmit: function () {
                    var form = $(plugins.rdMailForm[this.extraData.counter]);
                    var inputs = form.find("[data-constraints]");
                    if (isValidated(inputs)) {
                        var output = $("#" + form.attr("data-form-output"));

                        if (output.hasClass("snackbars")) {
                            output.html('<p><span class="icon text-middle fa fa-circle-o-notch fa-spin icon-xs"></span><span>Sending</span></p>');
                            output.addClass("active");
                        }
                    } else {
                        return false;
                    }
                },
                error: function (result) {
                    var output = $("#" + $(plugins.rdMailForm[this.extraData.counter]).attr("data-form-output"));
                    output.text(msg[result]);
                },
                success: function (result) {
                    var form = $(plugins.rdMailForm[this.extraData.counter]);
                    var output = $("#" + form.attr("data-form-output"));
                    form.addClass('success');
                    result = result.length == 5 ? result : 'MF255';
                    output.text(msg[result]);
                    if (result === "MF000") {
                        if (output.hasClass("snackbars")) {
                            output.html('<p><span class="icon text-middle mdi mdi-check icon-xs"></span><span>' + msg[result] + '</span></p>');
                        } else {
                            output.addClass("success");
                            output.addClass("active");
                        }
                    } else {
                        if (output.hasClass("snackbars")) {
                            output.html(' <p class="snackbars-left"><span class="icon icon-xs mdi mdi-alert-outline text-middle"></span><span>' + msg[result] + '</span></p>');
                        } else {
                            output.addClass("error");
                            output.addClass("active");
                        }
                    }
                    form.clearForm();
                    form.find('input, textarea').blur();

                    setTimeout(function () {
                        output.removeClass("active");
                        form.removeClass('success');
                    }, 5000);
                }
            });
        }
    }

    /**
     * RD Range
     * @description Enables RD Range plugin
     */
    if (plugins.rdRange.length) {
        plugins.rdRange.RDRange({});
    }

    /**
     * PhotoSwipe Gallery
     * @description Enables PhotoSwipe Gallery plugin
     */
    if (plugins.photoSwipeGallery.length) {

        // init image click event
        $document.delegate("[data-photo-swipe-item]", "click", function (event) {
            event.preventDefault();

            var $el = $(this),
                $galleryItems = $el.parents("[data-photo-swipe-gallery]").find("a[data-photo-swipe-item]"),
                pswpElement = document.querySelectorAll('.pswp')[0],
                encounteredItems = {},
                pswpItems = [],
                options,
                pswpIndex = 0,
                pswp;

            if ($galleryItems.length == 0) {
                $galleryItems = $el;
            }

            // loop over the gallery to build up the photoswipe items
            $galleryItems.each(function () {
                var $item = $(this),
                    src = $item.attr('href'),
                    size = $item.attr('data-size').split('x'),
                    pswdItem;

                if ($item.is(':visible')) {

                    // if we have this image the first time
                    if (!encounteredItems[src]) {
                        // build the photoswipe item
                        pswdItem = {
                            src: src,
                            w: parseInt(size[0], 10),
                            h: parseInt(size[1], 10),
                            el: $item // save link to element for getThumbBoundsFn
                        };

                        // store that we already had this item
                        encounteredItems[src] = {
                            item: pswdItem,
                            index: pswpIndex
                        };

                        // push the item to the photoswipe list
                        pswpItems.push(pswdItem);
                        pswpIndex++;
                    }
                }
            });

            options = {
                index: encounteredItems[$el.attr('href')].index,

                getThumbBoundsFn: function (index) {
                    var $el = pswpItems[index].el,
                        offset = $el.offset();

                    return {
                        x: offset.left,
                        y: offset.top,
                        w: $el.width()
                    };
                }
            };

            // open the photoswipe gallery
            pswp = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, pswpItems, options);
            pswp.init();
        });
    }


    /**
     * Stacktable
     * @description Enables Stacktable plugin
     */
    if (plugins.stacktable.length) {
        var i;
        for (i = 0; i < plugins.stacktable.length; i++) {
            var stacktableItem = $(plugins.stacktable[i]);
            stacktableItem.stacktable();
        }
    }

    /**
     * Custom Toggles
     */
    if (plugins.customToggle.length) {
        var i;
        for (i = 0; i < plugins.customToggle.length; i++) {
            var $this = $(plugins.customToggle[i]);
            $this.on('click', function (e) {
                e.preventDefault();
                $("#" + $(this).attr('data-custom-toggle')).add(this).toggleClass('active');
            });

            if ($this.attr("data-custom-toggle-disable-on-blur") === "true") {
                $("body").on("click", $this, function (e) {
                    if (e.target !== e.data[0] && $("#" + e.data.attr('data-custom-toggle')).find($(e.target)).length == 0 && e.data.find($(e.target)).length == 0) {
                        $("#" + e.data.attr('data-custom-toggle')).add(e.data[0]).removeClass('active');
                    }
                })
            }
        }
    }

    /**
     * Magnificent image zoom
     */
    if (plugins.imgZoom.length) {
        var i;
        for (i = 0; i < plugins.imgZoom.length; i++) {
            var $imgZoomItem = $(plugins.imgZoom[i]);
            $imgZoomItem.mag();
        }
    }

    /**
     * Custom Waypoints
     */
    if (plugins.customWaypoints.length) {
        var i;
        for (i = 0; i < plugins.customWaypoints.length; i++) {
            var $this = $(plugins.customWaypoints[i]);

            $this.on('click', function (e) {
                e.preventDefault();
                $("body, html").stop().animate({
                    scrollTop: $("#" + $(this).attr('data-custom-scroll-to')).offset().top
                }, 1000, function () {
                    $(window).trigger("resize");
                });
            });
        }
    }


    if (plugins.calcSlider1.length) {

        var calcSlider2 = $(".calc-slider-2");
        var calcSlider3 = $(".calc-slider-3");
        var calcSlider0 = $(".calc-slider-0");

        var cs1 = plugins.calcSlider1.find('.slider');
        var cv1 = plugins.calcSlider1.find('.value');

        var n1 = plugins.calcSlider1.find('.number');

        var cs0 = calcSlider0.find('.slider');
        var cv0 = calcSlider0.find('.value');
        var n0 = calcSlider0.find('.number');

        var cs2 = calcSlider2.find('.slider');
        var cv2 = calcSlider2.find('.value');
        var n2 = calcSlider2.find('.number');

        var cs3 = calcSlider3.find('.slider');
        var cv3 = calcSlider3.find('.value');
        var n3 = calcSlider3.find('.number');

        var a2 = 0.819985;
        var a3 = 0.005;

        var b7 = 0;
        var b8 = 0;
        var b9 = 0;


        var b11 = 0;
        var b12 = 0;
        var b13 = 0;

        var format = function (a) {
            a = Math.ceil(a)
            return String(a).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
        }

        var Count = function () {
            b11 = b7 * a2 - b7 * a3;
            b13 = b11 * b8 * b9 / 365 + b7 * a3;
            b12 = b7 - b11 - b13;


            n1.html(format(b11));
            n2.html(format(b12));
            n3.html(format(b13));

        };
        noUiSlider.create(cs0[0], {
            start: [16000000],
            step: 100000,
            range: {
                'min': 300000,
                'max': 30000000
            }
        });

        noUiSlider.create(cs1[0], {
            start: [16000000],
            step: 100000,
            range: {
                'min': 300000,
                'max': 30000000
            }
        });
        noUiSlider.create(cs2[0], {
            start: [5],
            step: 0.1,
            range: {
                'min': 1,
                'max': 30
            }
        });
        noUiSlider.create(cs3[0], {
            start: [5],
            step: 1,
            range: {
                'min': 1,
                'max': 180
            }
        });
        b7 = cs1[0].noUiSlider.get();
        b8 = cs2[0].noUiSlider.get() / 100;
        b9 = cs3[0].noUiSlider.get();

        Count();
        cs1[0].noUiSlider.on('update', function (values, handle) {
            cv1.html(format(values[handle]) + ' <img src="../../static/img/svg/ruble_medium_black.svg" width="16">');
            b7 = cs1[0].noUiSlider.get();
            Count();
        });


        cs2[0].noUiSlider.on('update', function (values, handle) {
            cv2.text(Math.round(values[handle] * 10) / 10 + '%');
            b8 = (cs2[0].noUiSlider.get()) / 100;
            Count();
        });


        cs3[0].noUiSlider.on('update', function (values, handle) {
            cv3.text(Math.round(values[handle] * 10) / 10 + ' д.');
            b9 = cs3[0].noUiSlider.get();
            Count();
        });
    }

    if ($("._calc").length) {

        (function () {
            var c = $("._calc")
            var slider = {};

            var start = {
                1: [16000000],
                2: [5],
                3: [5]
            };
            var step = {
                1: 100000,
                2: 0.01,
                3: 1
            };
            var range = {
                1: {
                    'min': 300000,
                    'max': 30000000
                },
                2: {
                    'min': 0.01,
                    'max': 0.3
                },
                3: {
                    'min': 1,
                    'max': 180
                }
            };
            var val = {
                1: 10000000,
                2: 0.18,
                3: 45,
            }
            var format = function (a) {
                a = Math.ceil(a)
                return String(a).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
            }
            for (var i = 1; i < 4; i++) {
                slider[i] = {};
                slider[i].el = c.find('._calc-slider-' + i).find('.slider');
                slider[i].t = c.find('._calc-slider-' + i).find('.value');
                slider[i].n = c.find('._calc-slider-' + i).find('.number');

                slider[i].val = val[i];
                noUiSlider.create(slider[i].el[0], {
                    start: val[i],
                    step: step[i],
                    range: range[i]
                });
            }
            var s0_val = {
                1: 0.25,
                2: 0.2,
                3: 0.15,
            }
            slider[0] = {};
            slider[0].val = s0_val[2];
            slider[0].el = c.find('._calc-slider-0').find('.number')

            var count = function () {
                var com = 0;
                var p = 0;
                var s = 0;
                s = (slider[1].val * slider[2].val * slider[3].val) / 365;
                com = s * slider[0].val;
                p = s - com;


                slider[1].n.html(format(com) + '<span>&nbsp;<i class="fa fa-rub fz-22" aria-hidden="true"></i></span>');
                slider[2].n.html(format(p)+ '<span>&nbsp;<i class="fa fa-rub fz-29" aria-hidden="true"></i></span>');
                var d = (slider[2].val*p/s)*100;
                slider[3].n.text(format(d)+' %')
            };
            c.find('._calc_check .p').on('click', function () {
                c.find('._calc_check').find('.p').removeClass('active');
                slider[0].val = s0_val[parseInt($(this).data('id'))];
                $(this).addClass('active');
                slider[0].el.text(slider[0].val * 100 + '%');
                count();
            })
            slider[1].el[0].noUiSlider.on('update', function (values, handle) {
                slider[1].t.html(format(values[handle]) + '<span>&nbsp;<i class="fa fa-rub fz-18" aria-hidden="true"></i></span>');
                slider[1].val = slider[1].el[0].noUiSlider.get();
                count()
            });
            slider[2].el[0].noUiSlider.on('update', function (values, handle) {
                slider[2].t.text(Math.round(values[handle] * 100) + ' %.');
                slider[2].val = slider[2].el[0].noUiSlider.get();
                count()
            });
            slider[3].el[0].noUiSlider.on('update', function (values, handle) {
                slider[3].t.text(Math.round(values[handle] * 10) / 10 + ' д.');
                slider[3].val = slider[3].el[0].noUiSlider.get();
                count()
            });


        })()
    }

    if ($("._calc2").length) {

        (function () {
            var c = $("._calc2");
            var slider = {};

            var start = {
                1: [16000000],
                2: [5],
                3: [5],
                4: [5]
            };
            var step = {
                1: 100000,
                2: 1,
                3: 0.01,
                4: 0.01
            };
            var range = {
                1: {
                    'min': 300000,
                    'max': 30000000
                },
                2: {
                    'min': 1,
                    'max': 180
                },
                3: {
                    'min': 0.5,
                    'max': 0.95
                },
                4: {
                    'min': 0.01,
                    'max': 0.3
                }
            };
            var val = {
                1: 10000000,
                2: 60,
                3: 0.8,
                4: 0.1,
            }


            var format = function (a) {
                a = Math.ceil(a)
                return String(a).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
            }
            for (var i = 1; i < 5; i++) {
                slider[i] = {};
                slider[i].el = c.find('._calc2-slider-' + i).find('.slider');
                slider[i].t = c.find('._calc2-slider-' + i).find('.value');
                slider[i].n = c.find('._calc2-slider-' + i).find('.number');

                slider[i].val = val[i];
                noUiSlider.create(slider[i].el[0], {
                    start: val[i],
                    step: step[i],
                    range: range[i]
                });
            }
            var s0_val = {
                1: 0.25,
                2: 0.2,
                3: 0.15,
                4: 0.15,
            };
            // slider[0] = {};
            // slider[0].val = s0_val[2];
            // slider[0].el = c.find('._calc-slider-0').find('.number')
            //
            function limit() {
                var s0 = (slider[1].val * slider[2].val * slider[3].val * slider[4].val) / 365;
                var d1 = slider[3].val* slider[1].val + s0;
                var d2 = parseInt((d1/slider[1].val)*100);
                console.log(d2)
                if(d2>=100){
                    return false
                }else{
                    return true
                }
            }

            var count = function () {
                var com = 0;
                var p = 0;
                var s0, s1, s2 = 0;

                s0 = (slider[1].val * slider[2].val * slider[3].val * slider[4].val) / 365;
                // 	com = s*slider[0].val;
                // 	p = s - com;
                s2 = (slider[1].val * slider[3].val) - 1000;
                s1 = slider[1].val - s2 - s0;
                // slider[1].n.text(format(s0));
                // slider[1].n.text(format(s0));
                // slider[2].n.text(format(s1));
                // console.log(slider[1].val);
                // console.log(slider[2].val);
                // console.log(slider[3].val);
                // console.log(slider[4].val);
                // console.log(s1);
                // console.log(s2);
                // console.log(s0);
                // var d1 = slider[3].val* slider[1].val + s0;
                // var d2 = parseInt((d1/slider[1].val)*100);
                // console.log(d2)

                c.find('._calc2-sum').html(format(s2) + '<span>&nbsp;<i class="fa fa-rub fz-29" aria-hidden="true"></i></span>');
                slider[1].n.html(format(s0) + '<span>&nbsp;<i class="fa fa-rub fz-22" aria-hidden="true"></i></span>');
                slider[2].n.html(format(s1) + '<span>&nbsp;<i class="fa fa-rub fz-22" aria-hidden="true"></i></span>');

                // 	slider[2].n.text(format(p));
            };
            // c.find('._calc_check .p').on('click',function () {
            // 	c.find('._calc_check').find('.p').removeClass('active');
            // 	slider[0].val = s0_val[parseInt($(this).data('id'))];
            // 	$(this).addClass('active');
            // 	slider[0].el.text(slider[0].val*100+'%');
            // 	count();
            // })
            slider[1].el[0].noUiSlider.on('update', function (values, handle) {
                slider[1].t.html(format(values[handle]) + '<span>&nbsp;<i class="fa fa-rub fz-18" aria-hidden="true"></i></span>');
                slider[1].val = slider[1].el[0].noUiSlider.get();
                if(limit()) {
                    count()
                }
            });
            slider[2].el[0].noUiSlider.on('update', function (values, handle) {
                slider[2].t.text(Math.round(values[handle] * 10) / 10 + ' д.');
                slider[2].val = slider[2].el[0].noUiSlider.get();
                if(limit()) {
                    count()
                }
            });
            slider[3].el[0].noUiSlider.on('update', function (values, handle) {
                slider[3].t.text(Math.round(values[handle] * 100) + ' %.');
                slider[3].val = slider[3].el[0].noUiSlider.get();
                if(limit()) {
                    count()
                }
            });
            slider[4].el[0].noUiSlider.on('update', function (values, handle) {
                slider[4].t.text(Math.round(values[handle] * 100) + ' %.');
                slider[4].val = slider[4].el[0].noUiSlider.get();
                if(limit()) {
                    count()
                }
            });


        })()
    }
    $.fn.dragDiagonal = function (eles) {
        var mD = false, dragEle = this,
            dx = 0, dy = 0;
        dragEle.on("mousedown", function (e) {
            if (e.which == 1) {
                mD = true;
                dx = e.clientX - this.offsetLeft;
                dy = e.clientY - this.offsetTop;
            }
        });
        $(window).on("mousemove", function (e) {
            if (!mD) return false;

            var p = e.clientX - dx, q = e.clientY - dy,
                a1 = eles[0].offsetLeft, b1 = eles[0].offsetTop,
                a2 = eles[1].offsetLeft, b2 = eles[1].offsetTop,
                m = (b2 - b1) / (a2 - a1),
                m1 = -1 / m,
                i = (-m1 * p + q + m * a1 - b1) / (m - m1);
            if (a2 > a1) {
                if (i > a2) i = a2;
                if (i < a1) i = a1;
            } else {
                if (i > a1) i = a1;
                if (i < a2) i = a2;
            }
            var j = m * (i - a1) + b1;

            // var Opacity = 1-(j-b1)/(b2-b1)+0.15;

            dragEle.css({top: j, left: i});
            e.preventDefault();
            eles[2](j, i);

        }).mouseup(function () {
            mD = false;
        });
    }
    if ($("._calc3").length) {

        (function () {
            var c = $("._calc3");
            var slider = {};

            var start = {
                1: [16000000],
                2: [5],
                3: [5]
            };
            var step = {
                1: 100000,
                2: 1,
                3: 0.01
            };
            var range = {
                1: {
                    'min': 300000,
                    'max': 30000000
                },

                2: {
                    'min': 1,
                    'max': 180
                },
                3: {
                    'min': 0.01,
                    'max': 0.3
                },
            };
            var val = {
                1: 10000000,
                2: 69,
                3: 0.1,
            };
            var format = function (a) {
                a = Math.ceil(a);
                return String(a).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');
            };
            for (var i = 1; i < 4; i++) {
                slider[i] = {};
                slider[i].el = c.find('._calc-slider-' + i).find('.slider');
                slider[i].t = c.find('._calc-slider-' + i).find('.value');
                slider[i].n = c.find('._calc-slider-' + i).find('.number');

                slider[i].val = val[i];
                noUiSlider.create(slider[i].el[0], {
                    start: val[i],
                    step: step[i],
                    range: range[i]
                });
            }
            var s0_val = {
                1: 0.25,
                2: 0.2,
                3: 0.15,
            };
            slider[0] = {};
            slider[0].val = s0_val[2];
            slider[0].el = c.find('._calc-slider-0').find('.number');
            var graph = {};
            graph.y = 0.5;
            graph.x = 1;
            graph.el = c.find("._graph");
            var n = {};




            var count = function () {
                // var com = 0;
                // var p = 0;
                // var s = 0;
                var discount = slider[2].val * slider[3].val / 365;
                discount = parseFloat((discount * 100).toFixed(2));
                // var x =  slider[2].val;


                // console.log(y,x)
                var nd = slider[2].val * graph.x;
                n = {
                    1: slider[2].val-nd,
                    2:discount/(slider[2].val/nd)
                };
                var dolg = slider[1].val*(1-n[2]/100);


                var komisia = (slider[1].val-dolg)*slider[0].val;
                var vigod = slider[1].val-dolg-komisia;

                // console.log(slider[1].val)
                // console.log(n)
                // console.log(n)
                // console.log('discont',n[2]);
                // console.log('dolg',dolg);
                // console.log('komisia',komisia);
                // console.log('vigod',vigod);
                graph.el.find('._discont').text( parseFloat(n[2].toFixed(2))+'%');
                c.find('._dolg').text(format(dolg));
                c.find('._srok').text(parseInt(n[1])+' д.');
            
                c.find('._dolg').html(format(dolg) + '<span>&nbsp;<i class="fa fa-rub fz-22" aria-hidden="true"></i></span>');
                c.find('._comision').html(format(komisia) + '<span>&nbsp;<i class="fa fa-rub fz-29" aria-hidden="true"></i></span>');
                c.find('._vgod').html(format(vigod) + '<span>&nbsp;<i class="fa fa-rub fz-29" aria-hidden="true"></i></span>');
                // var dohod =(slider[3].val*vigod)/(slider[1].val-dolg);

                // console.log("++")
                // console.log(n[2])
                // console.log(slider[0].val)
                // console.log(slider[2].val)
                // console.log(n[1])
                // console.log("**")
                // var dohod = (n[2]/100)*(1-slider[0].val)*(365/(slider[2].val-(slider[2].val-n[1])));
                var dohod = (n[2]/100)*365*0.8/( slider[2].val);

                dohod = (isNaN(dohod))?0:dohod;
                if(isFinite(parseInt(dohod*100))) {
                    c.find('._dohod').html(parseFloat(dohod*100).toFixed(2) + '%');
                }
            };
            var discountCount = function () {
                var discount = slider[2].val * slider[3].val / 365;
                discount = parseFloat((discount * 100).toFixed(2));

                graph.el.find('.y').html(
                    '<div class="c">'+parseFloat(discount).toFixed(2)+'%</div>'+
                    '<div class="c">'+parseFloat(discount*0.75).toFixed(2)+'%</div>' +
                    '<div class="c">'+parseFloat(discount*0.5).toFixed(2)+'%</div>' +
                    '<div class="c">'+parseFloat(discount*0.25).toFixed(2)+'%</div>' +
                    '<div class="c">'+0+'</div>'
                );

                return discount
            };
            c.find('._calc_check .p').on('click', function () {
                c.find('._calc_check').find('.p').removeClass('active');
                slider[0].val = s0_val[parseInt($(this).data('id'))];
                $(this).addClass('active');
                slider[0].el.text(slider[0].val * 100 + '%');
                count();
            });
            var dig =  c.find(".dig");
            c.find(".drag").dragDiagonal([c.find(".start")[0], c.find(".end")[0], function (y, x) {
                    graph.x = parseFloat(((x*100/dig.width())/100).toFixed(2));
                    graph.x = 1 - graph.x;
                    // console.log(graph.x);
                    count()
                }]
            );

            slider[1].el[0].noUiSlider.on('update', function (values, handle) {
                slider[1].t.html(format(values[handle]) + '<span>&nbsp;<i class="fa fa-rub fz-18" aria-hidden="true"></i></span>');
                slider[1].val = slider[1].el[0].noUiSlider.get();
                count()
            });

            slider[2].el[0].noUiSlider.on('update', function (values, handle) {
                slider[2].t.text(Math.round(values[handle] * 10) / 10 + ' д.');
                slider[2].val = slider[2].el[0].noUiSlider.get();
                graph.el.find('.x').html(
                    '<div class="c">'+0+'</div>' +
                    '<div class="c">'+parseInt(slider[2].val*0.25)+'</div>' +
                    '<div class="c">'+parseInt(slider[2].val*0.5)+'</div>' +
                    '<div class="c">'+parseInt(slider[2].val*0.75)+'</div>' +
                    '<div class="c">'+parseInt(slider[2].val)+'</div>'
                );
                discountCount();
                count()
            });
            slider[3].el[0].noUiSlider.on('update', function (values, handle) {
                slider[3].t.text(Math.round(values[handle] * 100) + ' %.');
                slider[3].val = slider[3].el[0].noUiSlider.get();

                discountCount();
                count()
            });
            var date = function () {

                var el = c.find('._date');
                var btn = c.find('._date-btn');
                var pop = c.find('._date-pop');
                function addDays(date, days) {
                    var result = new Date(date);
                    result.setDate(result.getDate() + days);
                    return result;
                }
                var _MS_PER_DAY = 1000 * 60 * 60 * 24;
                function dateDiffInDays(a, b) {
                    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
                    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

                    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
                }
                el.datepicker({
                    minDate:new Date(),
                    maxDate:addDays(new Date(),180),
                    onSelect:   function (formattedDate, date, inst) {
                        slider[2].el[0].noUiSlider.set(dateDiffInDays(new Date(),date));
                        pop.toggleClass('active')
                    }
                });


                btn.on('click',function () {
                    pop.toggleClass('active')
                })


            }();

        })()
    }
    // function ValidateForm(a, b) {
    //
    // 	a.validate({
    // 		errorElement: "span",
    // 		errorClass: "form-validation",
    // 		errorPlacement: function (error, element) {
    // 			error.appendTo(element.parent());
    // 		},
    // 		highlight: function (element, errorClass, validClass) {
    // 			return false;
    // 		},
    // 		unhighlight: function (element, errorClass, validClass) {
    // 			return false;
    // 		},
    // 		submitHandler: function (form) {
    // 			b()
    // 		}
    // 	});
    // };
    // function SendForms(c,s){
    // 	var f1_d = c.find('.form1').serializeArray();
    // 	var f2_d = c.find('.form2').serializeArray();
    // 	DATA = f1_d.concat(f2_d);
    //
    // 	/*
    // 	 [{"name":"f_type","value":"invest"},
    // 	 {"name":"f1-name","value":"1"},
    // 	 {"name":"f1-inn","value":"1"},
    // 	 {"name":"f1-phone","value":"11"},
    // 	 {"name":"f1-org_name","value":"1"},
    // 	 {"name":"f1-ogrn","value":"1"},
    // 	 {"name":"f1-email","value":"1"},
    // 	 {"name":"rezident","value":"on"},
    // 	 {"name":"f2-fio","value":"1"},
    // 	 {"name":"f2-citizenship","value":"1"},
    // 	 {"name":"f2-birthday","value":"01-10-2016"},
    // 	 {"name":"f2-phone","value":"1"},
    // 	 {"name":"f2-email","value":"1"},
    // 	 {"name":"f2-pass","value":"12"}]
    //
    //
    // 	 */
    // 	$.ajax({
    // 		url: c.find('.form1').attr('actions'),
    // 		data: JSON.stringify(DATA),
    // 		method: "POST",
    // 		success: function () {
    // 			c.trigger('next.owl.carousel');
    // 			s()
    // 		},
    // 		error: function () {
    //
    // 		}
    // 	});
    // }
    // $.validator.messages.required = 'Поле обязательно';


    if (plugins.owlForms.length) {

        // var DATA = {};

        // $.validator.messages.required = 'Поле обязательно';
        //
        //
        // //plugins.owlForms.find('.item').width($(".responsive-tabs").width()+30)
        //
        // plugins.owlForms.owlCarousel({
        // 	loop: false,
        // 	nav: false,
        // 	mouseDrag: false,
        // 	touchDrag: false,
        // 	dots: false,
        // 	//autoWidth:true,
        // 	items: 1
        // });
        //
        // var reg_steps = $(".reg-steps");
        // var cleanSteps = function () {
        // 	reg_steps.find('.b').removeClass('active');
        // 	reg_steps.find('.r').removeClass('active');
        // 	reg_steps.find('.r').removeClass('complete');
        // 	reg_steps.find('.s').removeClass('active');
        //
        //
        // }
        // var firstStep = function () {
        // 	cleanSteps();
        // 	reg_steps.find('.r1').addClass('active');
        // 	reg_steps.find('.s1').addClass('active');
        // 	reg_steps.removeClass('complete')
        // }
        // var secondStep = function () {
        // 	cleanSteps();
        // 	reg_steps.find('.r1').addClass('complete');
        // 	reg_steps.find('.s2').addClass('active');
        // 	reg_steps.find('.r2').addClass('active');
        // 	reg_steps.find('.b1').addClass('active');
        // 	reg_steps.addClass('complete')
        // }
        // var thirdStep = function () {
        // 	cleanSteps();
        // 	reg_steps.find('.r1').addClass('complete');
        // 	reg_steps.find('.r2').addClass('complete');
        // 	reg_steps.find('.b1').addClass('active');
        // 	reg_steps.find('.b2').addClass('active');
        // 	reg_steps.find('.r3').addClass('active');
        // 	reg_steps.find('.s3').addClass('active');
        // 	reg_steps.addClass('complete')
        // };
        //
        // firstStep();
        //
        // var f1 = new ValidateForm(
        // 	plugins.owlForms.find('.form1'),
        // 	function () {
        // 		secondStep();
        //
        // 		plugins.owlForms.trigger('next.owl.carousel');
        // 	});
        // var f2 = new ValidateForm(
        // 	plugins.owlForms.find('.form2'),
        // 	function () {
        //
        // 		SendForms(plugins.owlForms,thirdStep)
        //
        //
        //
        // 	});
        // plugins.owlForms.find('.return-btn').on('click', function () {
        // 	plugins.owlForms.trigger('prev.owl.carousel');
        // 	firstStep();
        // });
        //
        // plugins.owlForms.find('.find_inn').on('click', function () {
        // 	var inn = $("#f1-inn").val();
        // 	console.log(inn);
        // 	if (inn) {
        // 		$.ajax({
        // 			url: '/find_inn',
        // 			data: inn,
        // 			method: "POST",
        // 			success: function (data) {
        // 				//var js = {
        // 				//    "name": "mkyong",
        // 				//    "phone": 30,
        // 				//    "org_name": '123123',
        // 				//    "ogrn":'123123'
        // 				//}
        // 				var js = JSON.parse(data);
        //
        // 				$("#f1-name").val(js.name);
        // 				$("#f1-phone").val(js.phone);
        // 				$("#f1-org_name").val(js.org_name);
        // 				$("#f1-ogrn").val(js.ogrn);
        //
        // 			},
        // 			error: function () {
        //
        // 			}
        // 		});
        // 	}
        //
        // });
        //
        // pickmeup($(".form-control-date")[0]);


    }


    function OwlLkReg() {
        var _this = this;

        var DATA = {};

        this.owl = plugins.owlLkReg;

        this.owl.owlCarousel({
            loop: false,
            nav: false,
            mouseDrag: false,
            touchDrag: false,
            dots: false,
            //autoWidth:true,
            items: 1
        });

        var reg_steps = $(".reg-steps");


        var cleanSteps = function () {
            reg_steps.find('.b').removeClass('active');
            reg_steps.find('.r').removeClass('active');
            reg_steps.find('.r').removeClass('complete');
            reg_steps.find('.s').removeClass('active');
            reg_steps.find('.r1').addClass('complete');
            reg_steps.find('.s1').addClass('complete');
            reg_steps.find('.b1').addClass('active');
            reg_steps.addClass('complete')

        }
        var firstStep = function () {
            cleanSteps();
            reg_steps.find('.r2').addClass('active');
            reg_steps.find('.s2').addClass('active');
        }
        var secondStep = function () {
            cleanSteps();
            reg_steps.find('.r2').addClass('complete');
            reg_steps.find('.s2').addClass('complete');


            reg_steps.find('.s3').addClass('active');
            reg_steps.find('.r3').addClass('active');
            reg_steps.find('.b2').addClass('active');


        }
        var thirdStep = function () {
            cleanSteps();
            reg_steps.find('.r2').addClass('complete');
            reg_steps.find('.r2').addClass('complete');

            reg_steps.find('.b1').addClass('active');
            reg_steps.find('.b2').addClass('active');
            reg_steps.find('.b3').addClass('active');

            reg_steps.find('.r3').addClass('complete');
            reg_steps.find('.s3').addClass('complete');


            reg_steps.find('.r4').addClass('active');
            reg_steps.find('.s4').addClass('active');

            reg_steps.addClass('complete')
        };

        firstStep();

        var f1 = new ValidateForm(
            this.owl.find('.form1'),
            function () {
                secondStep();

                _this.owl.trigger('next.owl.carousel');
            });
        var f2 = new ValidateForm(
            this.owl.find('.form2'),
            function () {


                //var f1_d = plugins.owlForms.find('.form1').serializeArray();
                //var f2_d = plugins.owlForms.find('.form2').serializeArray();
                //DATA = f1_d.concat(f2_d);

                /*
                 [{"name":"f_type","value":"invest"},
                 {"name":"f1-name","value":"1"},
                 {"name":"f1-inn","value":"1"},
                 {"name":"f1-phone","value":"11"},
                 {"name":"f1-org_name","value":"1"},
                 {"name":"f1-ogrn","value":"1"},
                 {"name":"f1-email","value":"1"},
                 {"name":"rezident","value":"on"},
                 {"name":"f2-fio","value":"1"},
                 {"name":"f2-citizenship","value":"1"},
                 {"name":"f2-birthday","value":"01-10-2016"},
                 {"name":"f2-phone","value":"1"},
                 {"name":"f2-email","value":"1"},
                 {"name":"f2-pass","value":"12"}]


                 */
                //$.ajax({
                //	url: plugins.owlForms.find('.form1').attr('actions'),
                //	data: JSON.stringify(DATA),
                //	method: "POST",
                //	success: function () {


                //	},
                //	error: function () {
                //
                //	}
                //});

                SendForms(_this.owl, thirdStep)
            });


        pickmeup($(".form-control-date")[0]);


    }


    if (plugins.owlLkReg.length) {

        var owlLkReg = new OwlLkReg();
    }


    $('.fileupload').fileupload({
        dataType: 'json',
        add: function (e, data) {


            //data.context = $('<p/>').text('Uploading...').appendTo(document.body);
            data.submit();


            var uploadErrors = [];
            var acceptFileTypes = /^image\/(gif|jpe?g|png|pdf|doc|txt)$/i;
            if (data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                uploadErrors.push('Not an accepted file type');
            }
            if (data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 10000000) {
                uploadErrors.push('Filesize is too big');
            }
            if (uploadErrors.length > 0) {

            } else {
                $(this).parent('.fileinput').addClass('load');
                data.submit();
            }


        },
        done: function (e, data) {
            $(this).parent('.fileinput').removeClass('load')
            $(this).parent('.fileinput').find('.drop').text(data.files[0].name);
        },
        error: function (e, data) {
            $(this).parent('.fileinput').removeClass('load')
        }
    });

    if (plugins.hwp1.length) {
        var hwp_1_l = $("._hwp_1_l")
        var hwp_1_r = $("._hwp_1_r")
        var hwp_1_i = $("._hwp_1_i")
        var hwp_1_ul = $("._hwp_1_ul")
        hwp_1_r.slick({
            dots: false,
            infinite: false,
            speed: 200,
            fade: true,
            cssEase: 'linear',
            arrows: false
        });
        hwp_1_i.slick({
            dots: false,
            infinite: false,
            speed: 200,
            fade: true,
            cssEase: 'linear',
            arrows: false
        });
        hwp_1_l.slick({
            dots: false,
            infinite: false,
            speed: 200,
            fade: true,
            cssEase: 'linear',
            arrows: false
        });
        var hwp_id = 1;
        var hwp_size = hwp_1_ul.find('li').length
        var timeout = function () {
            setTimeout(function () {

                hwp_1_ul.find('li').removeClass('active');
                if (hwp_id + 1 > hwp_size) {
                    hwp_id = 1;
                } else {
                    hwp_id = hwp_id + 1;
                }
                hwp_1_ul.find('.li_' + hwp_id).addClass('active');
                hwp_1_l.slick('slickGoTo', hwp_id - 1);
                hwp_1_i.slick('slickGoTo', hwp_id - 1);
                hwp_1_r.slick('slickGoTo', hwp_id - 1);
                timeout();
            }, 8000);
        }
        timeout()

        hwp_1_ul.on('click', 'li', function () {

            hwp_1_ul.find('li').removeClass('active');

            var id = $(this).data('id');
            hwp_id = id;
            $(this).addClass('active');
            hwp_1_l.slick('slickGoTo', id - 1);
            hwp_1_i.slick('slickGoTo', id - 1);
            hwp_1_r.slick('slickGoTo', id - 1);


        })


    }
    if (plugins.hwp_quest.length) {
        var hwp_quest = $("._hwp_quest");
        var hwp_answer = $("._hwp_answer");
        // hwp_answer.find('.el').hide();
        var id = 0;
        var answers = hwp_answer.find('.answer_0')

        hwp_answer.slick({
            speed: 200,
            fade: true,
            cssEase: 'linear',
            arrows: false,
            autoplay: false,
            draggable: false
        })
        hwp_answer.slick('slickGoTo', id);


        for (var i = 0; i < hwp_answer.find('.el').length; i++) {

            hwp_answer.find('.answer_' + i).slick({
                dots: true,
                infinite: false,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000,
                draggable: false
            })
        }

        hwp_quest.on('click', '.el', function () {
            hwp_quest.find('.el').removeClass('active')
            $(this).addClass('active')
            id = $(this).data('id');
            hwp_answer.slick('slickGoTo', id);
        })

    }


    $("._ar1").on('click', function () {
        $(this).find('.ac2').toggleClass('active');
        $(this).toggleClass('active');

        $("._ar1_sub_"+$(this).data('id')).slideToggle();
    })
    $("._ar0").on('click', function () {
        $(this).find('.ac1').toggleClass('active');
        $(this).toggleClass('active');

        $("._ar0_sub_"+$(this).data('id')).slideToggle();
    })
    
    // $("._ar2").on('click', function () {
    //     $(this).find('.ac2').toggleClass('active');
    //     $("._ar2_sub_"+$(this).data('id')).slideToggle();
    // })
	//
   
    
});

